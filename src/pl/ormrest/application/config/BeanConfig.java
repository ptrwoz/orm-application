package pl.ormrest.application.config;

import java.util.List;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import pl.ormrest.bootloader.generator.classes.ORMClass;
import pl.ormrest.bootloader.generator.classes.ORMClassManager;


@Configuration
@Profile({"application"})
public class BeanConfig
  implements BeanDefinitionRegistryPostProcessor
{
  public BeanConfig() {}
  
  public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory)
    throws BeansException
  {}
  
  public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry)
    throws BeansException
  {
    List<ORMClass> controllerList = ORMClassManager.oRMClassControllerList;
    for (ORMClass oRMClass : controllerList) {
      BeanDefinition beanDefinition = new RootBeanDefinition(oRMClass.getORMClass(), Autowire.NO.value(), true);
      registry.registerBeanDefinition(oRMClass.getName() + "Bean", beanDefinition);
    }
  }
}
