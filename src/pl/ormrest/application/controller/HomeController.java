package pl.ormrest.application.controller;

import java.util.List;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.ormrest.bootloader.generator.classes.ORMClass;
import pl.ormrest.bootloader.generator.classes.ORMClassManager;



@RestController
@Profile({"application"})
public class HomeController
{
  public HomeController() {}
  
  @RequestMapping(method={org.springframework.web.bind.annotation.RequestMethod.GET}, value={""})
  @ResponseBody
  public String home()
  {
    String logo = "=============[ ORM Application ]=============";
    String content = "<br/>";
    List<ORMClass> modelList = ORMClassManager.oRMClassModelList;
    for (ORMClass oRMClass : modelList) {
      content = content + "==========[" + UppserFirstLetter(oRMClass.getName()) + "]==========<br/>";
      content = content + "=<save>=/" + UppserFirstLetter(oRMClass.getName()) + "/" + " (PUT)<br/>";
      content = content + "=<edit>=/" + UppserFirstLetter(oRMClass.getName()) + "/" + " (POST)<br/>";
      content = content + "=<delete>=/" + UppserFirstLetter(oRMClass.getName()) + "/" + " (DELETE)<br/>";
      content = content + "=<getAll>=/" + UppserFirstLetter(oRMClass.getName()) + "/findAll/" + " (GET)<br/>";
      content = content + "=<getById>=/" + UppserFirstLetter(oRMClass.getName()) + "/{id}" + " (GET)<br/>";
    }
    return logo + content;
  }
  
  private String UppserFirstLetter(String word) {
    return Character.toUpperCase(word.charAt(0)) + word.substring(1);
  }
}
