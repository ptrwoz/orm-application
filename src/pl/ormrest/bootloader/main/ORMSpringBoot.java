package pl.ormrest.bootloader.main;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;












public class ORMSpringBoot
{
  public static String[] args = null;
  public static ConfigurableApplicationContext applicationContext = null;
  public static Class mainClass = null;
  
  public ORMSpringBoot(String[] args, Class mainClass) {
    args = args;
    applicationContext = null;
    mainClass = mainClass;
  }
  
  public int run() {
    applicationContext = SpringApplication.run(mainClass, args);
    return 1;
  }
}
