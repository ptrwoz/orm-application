package pl.ormrest.bootloader.main;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import pl.ormrest.bootloader.compiler.ClassCompiler;
import pl.ormrest.bootloader.generator.ControllerGenerator;
import pl.ormrest.bootloader.generator.DatabaseManager;
import pl.ormrest.bootloader.generator.FileManager;
import pl.ormrest.bootloader.generator.JavaFileGenerator;
import pl.ormrest.bootloader.generator.ModelGenerator;
import pl.ormrest.bootloader.generator.ORMGenerator;
import pl.ormrest.bootloader.generator.classes.ORMClass;
import pl.ormrest.bootloader.generator.classes.ORMClassManager;
import pl.ormrest.bootloader.properties.ORMProperties;
import pl.ormrest.bootloader.properties.ORMPropertiesObject;









public class ORMApplication
{
  private ORMProperties oRMProperties = null;
  private ORMGenerator oRMGenerator = null;
  
  private FileManager fileManager = null;
  private DatabaseManager databaseManager = null;
  private ORMClassManager oRMClassManager = null;
  
  public ORMApplication() throws SQLException, IOException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    System.out.println(">>ORMAPP<< Start init ORM-APPLICATION.");
    oRMProperties = new ORMProperties();
    fileManager = new FileManager();
    databaseManager = new DatabaseManager();
    databaseManager.initDataSource();
    databaseManager.initDatabaseMetaData();
    if (databaseManager.testDataSource() == 1) {
      oRMClassManager = new ORMClassManager();
      fileManager.deleteOldGenerateFiles(new File(ORMProperties.getoRMPropertiesObject().getProjectPath()));
      JavaFileGenerator javaFileGenerator = new JavaFileGenerator(ORMProperties.getoRMPropertiesObject().getProjectPath(),this.databaseManager.getDataBaseName());
      ModelGenerator modelClass = new ModelGenerator();
      ControllerGenerator controllerClass = new ControllerGenerator();
      ClassCompiler classCompiler = new ClassCompiler();
      oRMGenerator = new ORMGenerator(classCompiler, javaFileGenerator, modelClass, controllerClass);
      oRMGenerator.getModelGenerator().initTableSchemes(databaseManager.getDatabaseMetaData());
      oRMGenerator.getControllerGenerator().initRepositoryClassCode(oRMGenerator.getModelGenerator().getClassNameList());
      generateJavaFiles();
      compileJavaFiles();
      initORMClassList();
      databaseManager.getDataSource().getConnection().close();
    }
  }
  
  public ORMApplication(ORMPropertiesObject oRMPropertiesObject) throws SQLException, IOException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
  {
    System.out.println(">>ORMAPP<< Start init ORM-APPLICATION.");
    oRMProperties = new ORMProperties(oRMPropertiesObject);
    fileManager = new FileManager();
    databaseManager = new DatabaseManager();
    databaseManager.initDataSource();
    databaseManager.initDatabaseMetaData();
    if (databaseManager.testDataSource() == 1) {
      oRMClassManager = new ORMClassManager();
      fileManager.deleteOldGenerateFiles(new File(ORMProperties.getoRMPropertiesObject().getProjectPath()));
      JavaFileGenerator javaFileGenerator = new JavaFileGenerator(ORMProperties.getoRMPropertiesObject().getProjectPath(),this.databaseManager.getDataBaseName());
      ModelGenerator modelClass = new ModelGenerator();
      ControllerGenerator controllerClass = new ControllerGenerator();
      ClassCompiler classCompiler = new ClassCompiler();
      oRMGenerator = new ORMGenerator(classCompiler, javaFileGenerator, modelClass, controllerClass);
      oRMGenerator.getModelGenerator().initTableSchemes(databaseManager.getDatabaseMetaData());
      oRMGenerator.getControllerGenerator().initRepositoryClassCode(oRMGenerator.getModelGenerator().getClassNameList());
      generateJavaFiles();
      compileJavaFiles();
      initORMClassList();
      databaseManager.getDataSource().getConnection().close();
    }
  }
  
  public int initORMClassList() throws ClassNotFoundException
  {
    for (int i = 0; i < ORMClassManager.oRMClassModelList.size(); i++) {
      ((ORMClass)ORMClassManager.oRMClassModelList.get(i)).setORMClass(oRMGenerator.getClassCompiler().getClassByName(ORMProperties.getoRMPropertiesObject().getClassPackage() + "." + UppserFirstLetter(((ORMClass)ORMClassManager.oRMClassModelList.get(i)).getName())));
      ((ORMClass)ORMClassManager.oRMClassControllerList.get(i)).setORMClass(oRMGenerator.getClassCompiler().getClassByName(ORMProperties.getoRMPropertiesObject().getClassPackage() + "." + UppserFirstLetter(new StringBuilder().append(((ORMClass)ORMClassManager.oRMClassModelList.get(i)).getName()).append("Controller").toString())));
    }
    return 1;
  }
  
  public int generateJavaFiles()
  {
    fileManager.initFiles(oRMGenerator.getModelGenerator().getClassCodeList().size());
    for (int i = 0; i < oRMGenerator.getModelGenerator().getClassCodeList().size(); i++) {
      File modelFile = oRMGenerator.getJavaFileGenerator().createFileFromString((String)oRMGenerator.getModelGenerator().getClassCodeList().get(i), (String)oRMGenerator.getModelGenerator().getClassNameList().get(i));
      ORMClassManager.oRMClassModelList.add(new ORMClass((String)oRMGenerator.getModelGenerator().getClassNameList().get(i)));
      File controllerFile = oRMGenerator.getJavaFileGenerator().createFileFromString((String)oRMGenerator.getControllerGenerator().getRepositoryCodeList().get(i), (String)oRMGenerator.getControllerGenerator().getRepositoryNameList().get(i));
      ORMClassManager.oRMClassControllerList.add(new ORMClass((String)oRMGenerator.getControllerGenerator().getRepositoryNameList().get(i)));
      fileManager.getModelFiles()[i] = modelFile;
      fileManager.getControllerFiles()[i] = controllerFile;
    }
    return 1;
  }
  
  private String getMainPath(String projectPath, String packageName) {
    int packageNamePartSize = packageName.split("\\.").length;
    String[] projectPathArray = projectPath.split("\\\\");
    String result = "";
    for (int i = 0; i < projectPathArray.length - packageNamePartSize; i++) {
      result = result + projectPathArray[i] + "\\";
    }
    return result;
  }
  
  private int compileJavaFiles() throws IOException {
    String projectPath = ORMProperties.getoRMPropertiesObject().getProjectPath();
    String packageName = ORMProperties.getoRMPropertiesObject().getClassPackage();
    String basePath = getMainPath(projectPath, packageName);
    File parentDirectory = new File(basePath);
    File[] allFiles = fileManager.concatFileArray(fileManager.getModelFiles(), fileManager.getControllerFiles());
    oRMGenerator.getClassCompiler().compileFromFile(allFiles, parentDirectory);
    return 1;
  }
  
  private String UppserFirstLetter(String word) {
    return Character.toUpperCase(word.charAt(0)) + word.substring(1);
  }
}
