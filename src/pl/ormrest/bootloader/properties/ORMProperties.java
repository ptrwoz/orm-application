package pl.ormrest.bootloader.properties;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Properties;


public final class ORMProperties
{
  private static ORMPropertiesObject oRMPropertiesObject;
  
  public ORMProperties()
  {
    oRMPropertiesObject = new ORMPropertiesObject();
    initProperties();
  }
  
  public ORMProperties(ORMPropertiesObject oRMPropertiesObject) { oRMPropertiesObject = oRMPropertiesObject; }
  
  public int createProperties()
  {
    OutputStream output = null;
    try {
      Properties properties = new Properties();
      output = new FileOutputStream("ormconfig.properties");
      properties.setProperty("orm.driverClassName", "?");
      properties.setProperty("orm.url", "?");
      properties.setProperty("orm.username", "?");
      properties.setProperty("orm.password", "?");
      properties.setProperty("orm.projectPath", "?");
      properties.setProperty("orm.schema", "?");
      properties.setProperty("orm.classPackage", "?");
      properties.setProperty("orm.dialect", "?");
      properties.store(output, null);
      















      return 0;
    }
    catch (IOException eSaveError)
    {
      System.out.println(">>ORMAPP<< Error save ormconfig.properties.");
      System.out.println(eSaveError);
      return 0;
    } finally {
      if (output != null) {
        try {
          output.close();
          System.out.println(">>ORMAPP<< create ormconfig.properties.");
          return 1;
        } catch (IOException e) {
          System.out.println(">>ORMAPP<< Error save ormconfig.properties.");
          return 0;
        }
      }
    }
  }
  
  public int initProperties()
  {
    InputStream input = null;
    try {
      Properties properties = new Properties();
      input = new FileInputStream("ormconfig.properties");
      properties.load(input);
      oRMPropertiesObject.setDriverClassName(properties.getProperty("orm.driverClassName"));
      oRMPropertiesObject.setUrl(properties.getProperty("orm.url"));
      oRMPropertiesObject.setUsername(properties.getProperty("orm.username"));
      oRMPropertiesObject.setPassword(properties.getProperty("orm.password"));
      oRMPropertiesObject.setProjectPath(properties.getProperty("orm.projectPath"));
      oRMPropertiesObject.setSchema(properties.getProperty("orm.schema"));
      oRMPropertiesObject.setClassPackage(properties.getProperty("orm.classPackage"));
      oRMPropertiesObject.setClassPackage(properties.getProperty("orm.dialect"));
      
















      return 0;
    }
    catch (IOException eErrorLoad)
    {
      System.out.println(">>ORMAPP<< Error load ormconfig.properties.");
      System.out.println(eErrorLoad);
      return createProperties();
    } finally {
      if (input != null) {
        try {
          input.close();
          System.out.println(">>ORMAPP<< Success load ormconfig.properties.");
          return 1;
        } catch (IOException eErrorLoad) {
          System.out.println(">>ORMAPP<< Error load ormconfig.properties.");
          System.out.println(eErrorLoad);
          return 0;
        }
      }
    }
  }
  
  public static ORMPropertiesObject getoRMPropertiesObject()
  {
    return oRMPropertiesObject;
  }
}
