package pl.ormrest.bootloader.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Properties;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.ormrest.bootloader.main.ORMApplication;
import pl.ormrest.bootloader.main.ORMSpringBoot;
import pl.ormrest.bootloader.properties.ORMPropertiesObject;

@RestController
@Profile({"bootloader"})
public class BootloaderController
{
  public BootloaderController() {}
  
  private ConfigurableApplicationContext applicationContext = null;
  private boolean isRunningStadardApp = false;
  
  @RequestMapping(method={org.springframework.web.bind.annotation.RequestMethod.GET}, value={"stop"})
  @ResponseBody
  public String stopStandardApp() {
    if ((isRunningStadardApp) && (applicationContext != null)) {
      SpringApplication.exit(applicationContext, new ExitCodeGenerator[] { new BootloaderController(this) });
      




      isRunningStadardApp = false;
      applicationContext = null;
      return "=============[ ORM Application ]=============<br/>Standard App stoped...";
    }
    return "=============[ ORM Application ]=============<br/>Standard App is stop.";
  }
  
  @RequestMapping(method={org.springframework.web.bind.annotation.RequestMethod.GET}, value={"start"})
  @ResponseBody
  public String startStandardApp() throws SQLException, IOException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
  {
    if ((isRunningStadardApp) && (applicationContext != null)) {
      return "=============[ ORM Application ]=============<br/>Bootloader already running!";
    }
    isRunningStadardApp = true;
    try {
      ORMApplication oRMApplication = new ORMApplication();
      System.getProperties().put("server.port", Integer.valueOf(8081));
      System.getProperties().put("spring.profiles.active", "application");
      applicationContext = SpringApplication.run(ORMSpringBoot.mainClass, ORMSpringBoot.args);
      return "=============[ ORM Application ]=============<br/>Bootloader init run.";
    } catch (Exception e) {
      return "=============[ ORM Application ]=============<br/>Bootloader init error: " + e;
    }
  }
  
  @RequestMapping(method={org.springframework.web.bind.annotation.RequestMethod.PUT}, value={"start"})
  @ResponseBody
  public String startStandardApp(@org.springframework.web.bind.annotation.RequestBody ORMPropertiesObject oRMPropertiesObject) throws SQLException, IOException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
  {
    if ((isRunningStadardApp) && (applicationContext != null)) {
      return "=============[ ORM Application ]=============<br/>Bootloader already running!";
    }
    
    isRunningStadardApp = true;
    try {
      ORMApplication oRMApplication = new ORMApplication(oRMPropertiesObject);
      System.getProperties().put("server.port", Integer.valueOf(8081));
      System.getProperties().put("spring.profiles.active", "application");
      applicationContext = SpringApplication.run(ORMSpringBoot.mainClass, ORMSpringBoot.args);
      return "=============[ ORM Application ]=============<br/>Bootloader init run.";
    } catch (Exception e) {
      return "=============[ ORM Application ]=============<br/>Bootloader init error: " + e;
    }
  }
  

  @RequestMapping(method={org.springframework.web.bind.annotation.RequestMethod.GET}, value={""})
  @ResponseBody
  public String statusBootloader()
  {
    if ((isRunningStadardApp) && (applicationContext != null)) {
      return "=============[ ORM Application ]=============<br/>Bootloader server is running...<br/>Standard App working...";
    }
    return "=============[ ORM Application ]=============<br/>Bootloader server is running...<br/>Standard App not working.";
  }
}
