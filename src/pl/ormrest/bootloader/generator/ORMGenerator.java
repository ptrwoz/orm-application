package pl.ormrest.bootloader.generator;

import pl.ormrest.bootloader.compiler.ClassCompiler;










public class ORMGenerator
{
  private ClassCompiler classCompiler = null;
  private JavaFileGenerator javaFileGenerator = null;
  private ModelGenerator modelGenerator = null;
  private ControllerGenerator controllerGenerator = null;
  
  public ORMGenerator(ClassCompiler classCompiler, JavaFileGenerator javaFileGenerator, ModelGenerator modelGenerator, ControllerGenerator controllerGenerator) {
    this.classCompiler = classCompiler;
    this.javaFileGenerator = javaFileGenerator;
    this.modelGenerator = modelGenerator;
    this.controllerGenerator = controllerGenerator;
  }
  
  public JavaFileGenerator getJavaFileGenerator() {
    return javaFileGenerator;
  }
  
  public ModelGenerator getModelGenerator() {
    return modelGenerator;
  }
  
  public ControllerGenerator getControllerGenerator() {
    return controllerGenerator;
  }
  
  public ClassCompiler getClassCompiler() {
    return classCompiler;
  }
}
