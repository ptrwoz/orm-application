package pl.ormrest.bootloader.generator.classes;


public class ORMClass
{
  String name;
  
  Class<?> ORMClass;
  

  public ORMClass(String name)
  {
    this.name = name;
  }
  
  public String getName() {
    return name;
  }
  
  public Class<?> getORMClass() {
    return ORMClass;
  }
  
  public void setORMClass(Class<?> ORMClass) {
    this.ORMClass = ORMClass;
  }
}
