package pl.ormrest.bootloader.generator.classes;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

public class ORMRepository
{
  private final Class entityClass;
  @org.springframework.beans.factory.annotation.Autowired
  private EntityManager entityManager;
  
  public ORMRepository(Class entityClass)
  {
    this.entityClass = entityClass;
  }
  


  protected EntityManager getSession()
  {
    return entityManager;
  }
  
  public void save(Object entity) {
    EntityTransaction tr = null;
    try {
      tr = entityManager.getTransaction();
      tr.begin();
      entityManager.persist(entity);
      tr.commit();
    } catch (Exception e) {
      tr.rollback();
      throw new RuntimeException(e.getMessage());
    }
  }
  
  public void delete(Object entity)
  {
    EntityTransaction tr = null;
    try {
      tr = entityManager.getTransaction();
      tr.begin();
      Object f = entityManager.merge(entity);
      entityManager.remove(f);
      tr.commit();
    } catch (Exception e) {
      tr.rollback();
      throw new RuntimeException(e.getMessage());
    }
  }
  
  public void update(Object entity) {
    EntityTransaction tr = null;
    try {
      tr = entityManager.getTransaction();
      tr.begin();
      entityManager.merge(entity);
      tr.commit();
    } catch (Exception e) {
      tr.rollback();
      throw new RuntimeException(e.getMessage());
    }
  }
  
  public List<Object> findAll()
  {
    Query query = entityManager.createQuery("SELECT e FROM " + entityClass.getSimpleName() + " e");
    return query.getResultList();
  }
  
  public Object findById(int id) {
    return entityManager.find(entityClass, Integer.valueOf(id));
  }
}
