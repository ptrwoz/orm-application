package pl.ormrest.bootloader.generator;

import java.io.PrintStream;
import java.sql.SQLException;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class DatabaseManager
{
  static
  {
    throw new RuntimeException("Uncompilable source code - package pl.ormrest.bootloader.properties does not exist");
  }
  





  private DriverManagerDataSource dataSource = null;
  private java.sql.DatabaseMetaData databaseMetaData = null;
  
  public DatabaseManager() {
    dataSource = new DriverManagerDataSource();
  }
  
  public DriverManagerDataSource getDataSource() {
    return dataSource;
  }
  
  public java.sql.DatabaseMetaData getDatabaseMetaData() {
    return databaseMetaData;
  }
  
  public int initDatabaseMetaData() throws SQLException {
    if (dataSource != null) {
      databaseMetaData = dataSource.getConnection().getMetaData();
      return 1;
    }
    return 0;
  }
  
  public int testDataSource() throws SQLException
  {
    if ((!dataSource.getConnection().isClosed()) || (dataSource.getConnection() != null)) {
      System.out.println(">>ORMAPP<< Success connection data base.");
      return 1;
    }
    System.out.println(">>ORMAPP<< Error connection data base.");
    return 0;
  }
  
  public int initDataSource() throws SQLException
  {
    try {
      if (dataSource != null) {
        dataSource = new DriverManagerDataSource();
        throw new RuntimeException("Uncompilable source code - Erroneous tree type: <any>");
      }
      



      return 0;
    }
    catch (Exception eErrorProperties) {
      System.out.println(">>ORMAPP<< Error properties in ormconfig.properties.");
      System.out.println(eErrorProperties); }
    return 0;
  }
  
  public String getDataBaseName() throws SQLException
  {
    return databaseMetaData.getURL().split("/")[(databaseMetaData.getURL().split("/").length - 1)];
  }
}
