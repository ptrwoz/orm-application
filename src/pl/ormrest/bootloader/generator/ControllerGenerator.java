package pl.ormrest.bootloader.generator;

import java.util.List;

public class ControllerGenerator { static { throw new RuntimeException("Uncompilable source code - package pl.ormrest.bootloader.properties does not exist"); }
  






  private List<String> controllerNameList = null;
  private List<String> controllerCodeList = null;
  
  public int initRepositoryClassCode(List<String> className) {
    controllerNameList = new java.util.ArrayList();
    controllerCodeList = new java.util.ArrayList();
    int i = 0; if (i < className.size()) {
      controllerNameList.add((String)className.get(i) + "Controller");
      throw new RuntimeException("Uncompilable source code - Erroneous tree type: <any>");
    }
    









    return 1;
  }
  
  private String initPackage(String packageName) {
    return "package " + packageName + ";";
  }
  
  private String initImportLibrary() {
    return "import org.springframework.web.bind.annotation.RequestMapping;import org.springframework.web.bind.annotation.RequestMethod;import org.springframework.web.bind.annotation.ResponseBody;import org.springframework.web.bind.annotation.RestController;import org.springframework.web.bind.annotation.RequestBody;import java.util.List;import org.springframework.web.bind.annotation.PathVariable;import pl.ormrest.bootloader.generator.classes.ORMRepository;";
  }
  






  private String initConstructorMethod(String name)
  {
    return "public " + UppserFirstLetter(name) + "Controller() { super(" + UppserFirstLetter(name) + ".class ); }";
  }
  
  private String initCreateMethod(String name)
  {
    return "@RequestMapping(method = RequestMethod.PUT, value = \"/" + UppserFirstLetter(name) + "/\")" + "public @ResponseBody String save" + UppserFirstLetter(name) + "(@RequestBody " + UppserFirstLetter(name) + " e) { save(e); return \"1\";}";
  }
  

  private String initUpdateMethod(String name)
  {
    return "@RequestMapping(method = RequestMethod.POST, value = \"/" + UppserFirstLetter(name) + "/\")" + "public @ResponseBody String update" + UppserFirstLetter(name) + "(@RequestBody " + UppserFirstLetter(name) + " e) { update(e); return \"1\";}";
  }
  

  private String initDeleteMethod(String name)
  {
    return "@RequestMapping(method = RequestMethod.DELETE, value = \"/" + UppserFirstLetter(name) + "/\")" + "public @ResponseBody String delete" + UppserFirstLetter(name) + "(@RequestBody " + UppserFirstLetter(name) + " e) { delete(e); return \"1\";}";
  }
  
  private String initFindAllMethod(String name)
  {
    return "@RequestMapping(method = RequestMethod.GET, value = \"/" + UppserFirstLetter(name) + "/findAll/\")" + "public @ResponseBody List<Object> findAll" + UppserFirstLetter(name) + "() {" + "return  findAll();}";
  }
  

  private String initFindById(String name)
  {
    return "@RequestMapping(method = RequestMethod.GET, value = \"/" + UppserFirstLetter(name) + "/{id}\")" + "public @ResponseBody Object findById" + UppserFirstLetter(name) + "(@PathVariable(value=\"id\") int id) {" + "return  findById(id);}";
  }
  
  public List<String> getRepositoryNameList()
  {
    return controllerNameList;
  }
  
  public void setRepositoryNameList(List<String> repositoryNameList) {
    controllerNameList = repositoryNameList;
  }
  
  public List<String> getRepositoryCodeList() {
    return controllerCodeList;
  }
  
  public void setRepositoryCodeList(List<String> repositoryCodeList) {
    controllerCodeList = repositoryCodeList;
  }
  
  private String UppserFirstLetter(String word) {
    return Character.toUpperCase(word.charAt(0)) + word.substring(1);
  }
  
  public ControllerGenerator() {}
}
