package pl.ormrest.bootloader.generator;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import pl.ormrest.bootloader.generator.database.FieldRelation;
import pl.ormrest.bootloader.generator.database.FieldScheme;
import pl.ormrest.bootloader.generator.database.TabelScheme;

public class ModelGenerator
{
  static
  {
    throw new RuntimeException("Uncompilable source code - package pl.ormrest.bootloader.properties does not exist");
  }
  






  private List<FieldRelation> fieldRelations = null;
  private List<TabelScheme> tabelSchemeList = null;
  private List<String> classNameList = null;
  private List<String> classCodeList = null;
  
  private boolean isPrimaryKey(List<String> primaryKeys, String columnName) throws SQLException {
    for (String string : primaryKeys) {
      if (string.equals(columnName)) {
        return true;
      }
    }
    return false;
  }
  
  private List<String> findPrimaryKeys(ResultSet resultPrimayKeys) throws SQLException {
    List<String> primaryKeyList = new ArrayList();
    while (resultPrimayKeys.next()) {
      primaryKeyList.add(resultPrimayKeys.getString(4));
    }
    return primaryKeyList;
  }
  
  private List<FieldRelation> getFieldRelationFromSourceTabel(String tabelName) {
    List<FieldRelation> tabelFieldRelationList = new ArrayList();
    for (FieldRelation fieldRelation : fieldRelations) {
      if (fieldRelation.getForeignKeySourceTabelName().equals(tabelName)) {
        tabelFieldRelationList.add(fieldRelation);
      }
    }
    return tabelFieldRelationList;
  }
  
  private List<FieldScheme> findFieldSchemes(String tabelName, ResultSet resultColumns, List<String> primaryKeyList) throws SQLException {
    List<FieldScheme> fieldSchemes = new ArrayList();
    List<FieldRelation> tabelFieldRelationList = getFieldRelationFromSourceTabel(tabelName);
    String columnType; while (resultColumns.next()) {
      columnType = resultColumns.getString("TYPE_NAME");
      String columnName = resultColumns.getString("COLUMN_NAME");
      FieldRelation currentRelation = null;
      for (FieldRelation fieldRelation : tabelFieldRelationList) {
        if ((fieldRelation.getRelationType() != pl.ormrest.bootloader.generator.database.FieldRelation.Relations.OneToMany) && 
          (fieldRelation.getForeignKeySourceFieldName().equals(columnName))) {
          currentRelation = fieldRelation;
        }
      }
      if (isPrimaryKey(primaryKeyList, columnName)) {
        fieldSchemes.add(new FieldScheme(columnType, columnName, true, currentRelation));
      } else {
        fieldSchemes.add(new FieldScheme(columnType, columnName, false, currentRelation));
      }
    }
    for (FieldRelation fieldRelation : tabelFieldRelationList) {
      if (fieldRelation.getRelationType() == pl.ormrest.bootloader.generator.database.FieldRelation.Relations.OneToMany) {
        fieldSchemes.add(new FieldScheme(fieldRelation.getForeignKeyDestinyTabelName(), fieldRelation.getForeignKeySourceFieldName(), false, fieldRelation));
      }
    }
    return fieldSchemes;
  }
  
  private int initRelationsWithoutType(DatabaseMetaData databaseMetaData, ResultSet tabelsData) throws SQLException {
    fieldRelations = new ArrayList();
    while (tabelsData.next()) {
      ResultSet foreignKeys = databaseMetaData.getImportedKeys("", "", tabelsData.getString(3));
      while (foreignKeys.next()) {
        String sourceTableName = foreignKeys.getString("FKTABLE_NAME");
        String sourceColumnName = foreignKeys.getString("FKCOLUMN_NAME");
        String destinyTableName = foreignKeys.getString("PKTABLE_NAME");
        String destinyColumnName = foreignKeys.getString("PKCOLUMN_NAME");
        FieldRelation fieldRelation = new FieldRelation(sourceColumnName, sourceTableName, destinyColumnName, destinyTableName);
        fieldRelations.add(fieldRelation);
      }
    }
    return 1;
  }
  
  private boolean isIndex(DatabaseMetaData databaseMetaData, String tabel, String column) throws SQLException {
    ResultSet index = databaseMetaData.getIndexInfo("", "", tabel, true, true);
    while (index.next()) {
      if ((!index.getString("NON_UNIQUE").equals(column)) && (index.getString("COLUMN_NAME").equals(column))) {
        return true;
      }
    }
    return false;
  }
  
  private int initRelationsWithType(DatabaseMetaData databaseMetaData) throws SQLException {
    List<FieldRelation> newFieldRelations = new ArrayList();
    for (int i = 0; i < fieldRelations.size(); i++) {
      for (int j = 0; j < fieldRelations.size(); j++) {
        if (((FieldRelation)fieldRelations.get(i)).getRelationType() != pl.ormrest.bootloader.generator.database.FieldRelation.Relations.None) break;
        ResultSet foreignKeys = databaseMetaData.getImportedKeys("", "", ((FieldRelation)fieldRelations.get(i)).getForeignKeyDestinyTabelName());
        boolean oneToOneFlag = false;
        while (foreignKeys.next()) {
          String sourceTableName = foreignKeys.getString("FKTABLE_NAME");
          String sourceColumnName = foreignKeys.getString("FKCOLUMN_NAME");
          String destinyTableName = foreignKeys.getString("PKTABLE_NAME");
          String str1 = foreignKeys.getString("PKCOLUMN_NAME");
        }
        












        ResultSet resultPrimayKeys = databaseMetaData.getPrimaryKeys("", "", ((FieldRelation)fieldRelations.get(i)).getForeignKeySourceTabelName());
        List<String> primaryKays = new ArrayList();
        primaryKays = findPrimaryKeys(resultPrimayKeys);
        ((FieldRelation)fieldRelations.get(i)).setRelationType(pl.ormrest.bootloader.generator.database.FieldRelation.Relations.ManyToOne);
        newFieldRelations.add(new FieldRelation(
          (String)primaryKays.get(0), 
          ((FieldRelation)fieldRelations.get(i)).getForeignKeyDestinyTabelName(), 
          ((FieldRelation)fieldRelations.get(i)).getForeignKeySourceFieldName(), 
          ((FieldRelation)fieldRelations.get(i)).getForeignKeySourceTabelName(), pl.ormrest.bootloader.generator.database.FieldRelation.Relations.OneToMany));
      }
    }
    




    for (FieldRelation newFieldRelation : newFieldRelations) {
      fieldRelations.add(newFieldRelation);
    }
    return 1;
  }
  
  public int initTableSchemes(DatabaseMetaData databaseMetaData) throws SQLException {
    tabelSchemeList = new ArrayList();
    classCodeList = new ArrayList();
    classNameList = new ArrayList();
    ResultSet tabelsData = databaseMetaData.getTables(null, "ething", "%", null);
    initRelationsWithoutType(databaseMetaData, tabelsData);
    initRelationsWithType(databaseMetaData);
    tabelsData = databaseMetaData.getTables(null, "ething", "%", null);
    while (tabelsData.next())
    {
      resultColumns = databaseMetaData.getColumns("", "", tabelsData.getString(3), "");
      ResultSet resultPrimayKeys = databaseMetaData.getPrimaryKeys("", "", tabelsData.getString(3));
      List<String> primaryKeyList = findPrimaryKeys(resultPrimayKeys);
      List<FieldScheme> fieldSchemes = findFieldSchemes(tabelsData.getString(3), resultColumns, primaryKeyList);
      if ((!fieldSchemes.isEmpty()) && (!primaryKeyList.isEmpty()))
      {

        TabelScheme tabelScheme = new TabelScheme(tabelsData.getString(3), fieldSchemes);
        
        tabelSchemeList.add(tabelScheme);
      }
    }
    ResultSet resultColumns = tabelSchemeList.iterator(); if (resultColumns.hasNext()) { TabelScheme tabelScheme = (TabelScheme)resultColumns.next();
      throw new RuntimeException("Uncompilable source code - Erroneous tree type: <any>");
    }
    

    return 1;
  }
  
  public List<String> getClassNameList() {
    return classNameList;
  }
  
  public void setClassNameList(List<String> classNameList) {
    this.classNameList = classNameList;
  }
  
  public List<String> getClassCodeList() {
    return classCodeList;
  }
  
  public void setClassCodeList(List<String> classCodeList) {
    this.classCodeList = classCodeList;
  }
  
  public String getDataBaseName(DatabaseMetaData databaseMetaData) throws SQLException {
    return databaseMetaData.getURL().split("/")[(databaseMetaData.getURL().split("/").length - 1)];
  }
  
  public ModelGenerator() {}
}
