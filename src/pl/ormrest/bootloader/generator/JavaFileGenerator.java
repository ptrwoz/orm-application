package pl.ormrest.bootloader.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;



public class JavaFileGenerator
{
  private final String directPath;
  
  public JavaFileGenerator(String directPath)
  {
    this.directPath = directPath;
    File theDir = new File(directPath);
    if (!theDir.exists()) {
      boolean result = false;
      try {
        theDir.mkdir();
      } catch (SecurityException se) {
        System.out.printf("safasf", new Object[0]);
      }
    }
  }
  
  public File createFileFromString(String sourceCode, String fileName) {
    try {
      File file = new File(directPath);
      File sourceFile = File.createTempFile(UppserFirstLetter(fileName), ".java", file);
      sourceFile.deleteOnExit();
      FileWriter writer = new FileWriter(sourceFile);Throwable localThrowable3 = null;
      try { writer.write(sourceCode);
      }
      catch (Throwable localThrowable1)
      {
        localThrowable3 = localThrowable1;throw localThrowable1;
      } finally {
        if (writer != null) if (localThrowable3 != null) try { writer.close(); } catch (Throwable localThrowable2) { localThrowable3.addSuppressed(localThrowable2); } else writer.close(); }
      File editFile = new File(directPath + "\\" + UppserFirstLetter(fileName) + ".java");
      sourceFile.renameTo(editFile);
      file.delete();
      return editFile;
    } catch (IOException ex) {
      Logger.getLogger(JavaFileGenerator.class.getName()).log(Level.SEVERE, null, ex); }
    return null;
  }
  
  String UppserFirstLetter(String word)
  {
    return Character.toUpperCase(word.charAt(0)) + word.substring(1);
  }
}
