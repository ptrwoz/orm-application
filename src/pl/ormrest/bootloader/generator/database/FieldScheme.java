package pl.ormrest.bootloader.generator.database;

import java.io.PrintStream;


public class FieldScheme
{
  private final String name;
  private final String sqlType;
  private final String javaType;
  private final boolean primaryKey;
  private FieldRelation fieldRelation;
  
  public FieldScheme(String type, String name, boolean primaryKey, FieldRelation fieldRelation)
  {
    this.name = name;
    sqlType = type;
    this.primaryKey = primaryKey;
    javaType = typeRecognition(type);
    this.fieldRelation = fieldRelation;
    if (type == null) {
      System.out.print("FieldScheme: Type Error!");
    }
  }
  
  public void setFieldRelation(FieldRelation fieldRelation) {
    this.fieldRelation = fieldRelation;
  }
  
  public String getJavaTypeAndName() {
    if (fieldRelation == null) {
      return javaType + " " + name;
    }
    return fieldRelation.getType() + " " + fieldRelation.getName();
  }
  
  public String getName()
  {
    if (fieldRelation == null) {
      return name;
    }
    return fieldRelation.getName();
  }
  
  public String getFieldCode()
  {
    if (fieldRelation != null) {
      return fieldRelation.getFieldRelationCode();
    }
    return javaType + " " + name + ";";
  }
  
  public String getFieldAnnotationsCode()
  {
    if (primaryKey)
      return "@Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name =\"" + name + "\")";
    if (fieldRelation != null) {
      return fieldRelation.getAnnotationCode();
    }
    return "@Column(name =\"" + name + "\")";
  }
  
  public String getGetterFieldCode()
  {
    if (fieldRelation != null) {
      return "public " + fieldRelation.getType() + " get" + UppserFirstLetter(fieldRelation.getName()) + "(){return this." + fieldRelation.getName() + ";}";
    }
    return "public " + javaType + " get" + UppserFirstLetter(name) + "(){return this." + name + ";}";
  }
  
  public String getSetterFieldCode()
  {
    if (fieldRelation != null) {
      return "public void set" + UppserFirstLetter(fieldRelation.getName()) + "(" + fieldRelation.getType() + " value " + "){this." + fieldRelation.getName() + " = value;}";
    }
    return "public void set" + UppserFirstLetter(name) + "(" + javaType + " value " + "){this." + name + " = value;}";
  }
  

  public String typeRecognition(String sqlType)
  {
    switch (sqlType) {
    case "CHAR": 
      return "String";
    case "VARCHAR": 
      return "String";
    case "LONGVARCHAR": 
      return "String";
    case "NUMERIC": 
      return "java.math.BigDecimal";
    case "DECIMAL": 
      return "java.math.BigDecimal";
    case "BIT": 
      return "boolean";
    case "TINYINT": 
      return "byte";
    case "SMALLINT": 
      return "short";
    case "MEDIUMINT": 
      return "int";
    case "INT": 
      return "int";
    case "INTEGER": 
      return "int";
    case "BIGINT": 
      return "long";
    case "REAL": 
      return "float";
    case "FLOAT": 
      return "double";
    case "DOUBLE": 
      return "double";
    case "BINARY": 
      return "byte[]";
    case "VARBINARY": 
      return "byte[]";
    case "LONGVARBINARY": 
      return "byte[]";
    case "TINYBLOB": 
      return "byte[]";
    case "BLOB": 
      return "byte[]";
    case "MEDIUMBLOB": 
      return "byte[]";
    case "LONGBLOB": 
      return "byte[]";
    case "TINYTEXT": 
      return "byte[]";
    case "TEXT": 
      return "byte[]";
    case "MEDIUMTEXT": 
      return "byte[]";
    case "LONGTEXT": 
      return "byte[]";
    case "ENUM": 
      return "byte[]";
    case "SET": 
      return "byte[]";
    case "DATE": 
      return "java.sql.Date";
    case "DATETIME": 
      return "java.sql.Date";
    case "TIME": 
      return "java.sql.Time";
    case "TIMESTAMP": 
      return "java.sql.Tiimestamp";
    case "YEAR": 
      return "java.sql.Date";
    case "text": 
      return "String";
    case "int4": 
      return "int";
    case "int8": 
      return "int";
    case "bytea": 
      return "byte[]";
    }
    return null;
  }
  
  public FieldRelation getFieldRelation()
  {
    return fieldRelation;
  }
  
  String UppserFirstLetter(String word) {
    return Character.toUpperCase(word.charAt(0)) + word.substring(1);
  }
}
