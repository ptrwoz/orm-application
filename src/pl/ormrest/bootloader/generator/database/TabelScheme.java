package pl.ormrest.bootloader.generator.database;

import java.io.PrintStream;
import java.sql.SQLException;
import java.util.List;




public class TabelScheme
{
  private String name = null;
  private List<FieldScheme> fieldSchemeList = null;
  private final String IMPORT_LIBRARY_CODE = "import java.io.Serializable;import javax.persistence.Column;import javax.persistence.JoinColumn;import javax.persistence.GenerationType;import javax.persistence.Entity;import javax.persistence.GeneratedValue;import java.util.Set;import javax.persistence.Id;import javax.persistence.FetchType;import javax.persistence.ManyToOne;import javax.persistence.Table;import javax.persistence.OneToOne;import javax.persistence.OneToMany;import com.fasterxml.jackson.annotation.JsonIdentityInfo;import com.fasterxml.jackson.annotation.ObjectIdGenerators;import com.fasterxml.jackson.annotation.JsonBackReference;import com.fasterxml.jackson.annotation.JsonManagedReference;import javax.persistence.CascadeType;";
  















  public TabelScheme(String name, List<FieldScheme> columns)
    throws SQLException
  {
    this.name = name;
    fieldSchemeList = columns;
    System.out.println(">>ORMAPP<< TabelScheme create: " + this.name);
  }
  
  private String generateClassBodyCode() { String constructorCode = "public " + UppserFirstLetter(name) + "(";
    String constructorBodyCode = "{";
    String fieldsCode = "";
    String emptyConstructor = "public " + UppserFirstLetter(name) + "(){}";
    for (FieldScheme fieldScheme : fieldSchemeList) {
      if (((FieldScheme)fieldSchemeList.get(fieldSchemeList.size() - 1)).equals(fieldScheme)) {
        constructorCode = constructorCode + fieldScheme.getJavaTypeAndName() + ")";
      } else {
        constructorCode = constructorCode + fieldScheme.getJavaTypeAndName() + ",";
      }
      constructorBodyCode = constructorBodyCode + "this." + fieldScheme.getName() + " = " + fieldScheme.getName() + ";";
      fieldsCode = fieldsCode + fieldScheme.getFieldAnnotationsCode();
      fieldsCode = fieldsCode + "private " + fieldScheme.getFieldCode();
      fieldsCode = fieldsCode + fieldScheme.getGetterFieldCode();
      fieldsCode = fieldsCode + fieldScheme.getSetterFieldCode();
    }
    constructorBodyCode = constructorBodyCode + "}";
    constructorCode = constructorCode + constructorBodyCode;
    return fieldsCode + constructorCode + emptyConstructor;
  }
  
  public String generateTabelCode(String packageCode) throws SQLException { String tabelPackageCode = "package " + packageCode + ";";
    getClass();String importLibraryCode = "import java.io.Serializable;import javax.persistence.Column;import javax.persistence.JoinColumn;import javax.persistence.GenerationType;import javax.persistence.Entity;import javax.persistence.GeneratedValue;import java.util.Set;import javax.persistence.Id;import javax.persistence.FetchType;import javax.persistence.ManyToOne;import javax.persistence.Table;import javax.persistence.OneToOne;import javax.persistence.OneToMany;import com.fasterxml.jackson.annotation.JsonIdentityInfo;import com.fasterxml.jackson.annotation.ObjectIdGenerators;import com.fasterxml.jackson.annotation.JsonBackReference;import com.fasterxml.jackson.annotation.JsonManagedReference;import javax.persistence.CascadeType;";
    String classAnnotationsCode = "@Entity @Table(name = \"" + name + "\") @JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property=\"@id\")";
    String classCode = "public class " + UppserFirstLetter(name) + " implements Serializable {";
    classCode = classCode + generateClassBodyCode() + "}";
    classCode = tabelPackageCode + importLibraryCode + classAnnotationsCode + classCode;
    return classCode;
  }
  
  public FieldScheme getFieldSchemeByName(String name) {
    for (FieldScheme fieldScheme : fieldSchemeList) {
      if (name.equals(fieldScheme.getName())) {
        return fieldScheme;
      }
    }
    return null;
  }
  
  public String getTabelSchemeName() {
    return name;
  }
  
  public List<FieldScheme> getFieldSchemeList() {
    return fieldSchemeList;
  }
  
  public void setFieldSchemeList(List<FieldScheme> fieldSchemeList) {
    this.fieldSchemeList = fieldSchemeList;
  }
  
  private String UppserFirstLetter(String word) {
    return Character.toUpperCase(word.charAt(0)) + word.substring(1);
  }
}
