package pl.ormrest.bootloader.generator.database;


public class FieldRelation
{
  private String foreignKeySourceFieldName;
  
  private String foreignKeySourceTabelName;
  
  private String foreignKeyDestinyFieldName;
  private String foreignKeyDestinyTabelName;
  private FieldRelation.Relations relationType;
  
  public FieldRelation(String foreignKeySourceFieldName, String foreignKeySourceTabelName, String foreignKeyDestinyFieldName, String foreignKeyDestinyTabelName, FieldRelation.Relations relationType)
  {
    this.foreignKeySourceFieldName = foreignKeySourceFieldName;
    this.foreignKeySourceTabelName = foreignKeySourceTabelName;
    this.foreignKeyDestinyFieldName = foreignKeyDestinyFieldName;
    this.foreignKeyDestinyTabelName = foreignKeyDestinyTabelName;
    this.relationType = relationType;
  }
  
  public FieldRelation(String foreignKeySourceFieldName, String foreignKeySourceTabelName, String foreignKeyDestinyFieldName, String foreignKeyDestinyTabelName) {
    this.foreignKeySourceFieldName = foreignKeySourceFieldName;
    this.foreignKeySourceTabelName = foreignKeySourceTabelName;
    this.foreignKeyDestinyFieldName = foreignKeyDestinyFieldName;
    this.foreignKeyDestinyTabelName = foreignKeyDestinyTabelName;
    relationType = FieldRelation.Relations.None;
  }
  
  public String getForeignKeySourceFieldName() {
    return foreignKeySourceFieldName;
  }
  
  public String getForeignKeySourceTabelName() {
    return foreignKeySourceTabelName;
  }
  
  public String getForeignKeyDestinyFieldName() {
    return foreignKeyDestinyFieldName;
  }
  
  public String getForeignKeyDestinyTabelName() {
    return foreignKeyDestinyTabelName;
  }
  
  public FieldRelation.Relations getRelationType() {
    return relationType;
  }
  
  public void setRelationType(FieldRelation.Relations relationType) {
    this.relationType = relationType;
  }
  



  public String getType()
  {
    if (relationType == FieldRelation.Relations.OneToMany) {
      return "Set<" + UppserFirstLetter(foreignKeyDestinyTabelName) + ">";
    }
    return UppserFirstLetter(foreignKeyDestinyTabelName);
  }
  
  public String getName()
  {
    if (relationType == FieldRelation.Relations.OneToMany) {
      return foreignKeyDestinyTabelName + "Set";
    }
    return foreignKeySourceFieldName;
  }
  
  public String getFieldRelationCode()
  {
    if (relationType == FieldRelation.Relations.OneToMany)
      return "Set<" + UppserFirstLetter(foreignKeyDestinyTabelName) + "> " + foreignKeyDestinyTabelName + "Set;";
    if (relationType == FieldRelation.Relations.ManyToOne) {
      return UppserFirstLetter(foreignKeyDestinyTabelName) + " " + foreignKeySourceFieldName + ";";
    }
    return UppserFirstLetter(foreignKeyDestinyTabelName) + " " + foreignKeySourceFieldName + ";";
  }
  

  public String getAnnotationCode()
  {
    if (relationType == FieldRelation.Relations.OneToMany)
      return "@JsonManagedReference @OneToMany( mappedBy = \"" + foreignKeyDestinyFieldName + "\", fetch = FetchType.EAGER)";
    if (relationType == FieldRelation.Relations.ManyToOne) {
      return "@JsonBackReference @JoinColumn(name = \"" + foreignKeySourceFieldName + "\", referencedColumnName = \"" + foreignKeyDestinyFieldName + "\")" + "@ManyToOne(fetch = FetchType.EAGER) ";
    }
    
    return "@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property=\"@id\")  @JoinColumn(name = \"" + foreignKeyDestinyFieldName + "\", referencedColumnName = \"" + foreignKeyDestinyFieldName + "\")" + "@OneToOne(fetch = FetchType.EAGER)";
  }
  

  public void setForeignKeySourceFieldName(String foreignKeySourceFieldName)
  {
    this.foreignKeySourceFieldName = foreignKeySourceFieldName;
  }
  
  public void setForeignKeySourceTabelName(String foreignKeySourceTabelName) {
    this.foreignKeySourceTabelName = foreignKeySourceTabelName;
  }
  
  public void setForeignKeyDestinyFieldName(String foreignKeyDestinyFieldName) {
    this.foreignKeyDestinyFieldName = foreignKeyDestinyFieldName;
  }
  
  public void setForeignKeyDestinyTabelName(String foreignKeyDestinyTabelName) {
    this.foreignKeyDestinyTabelName = foreignKeyDestinyTabelName;
  }
  
  private String UppserFirstLetter(String word) {
    return Character.toUpperCase(word.charAt(0)) + word.substring(1);
  }
}
