package pl.ormrest.bootloader.generator;

import java.io.File;








public class FileManager
{
  public FileManager() {}
  
  private File[] modelFiles = null;
  private File[] controllerFiles = null;
  
  public void deleteOldGenerateFiles(File directory) {
    File[] children = directory.listFiles();
    for (File child : children) {
      child.delete();
    }
  }
  
  public int initFiles(int size) {
    modelFiles = new File[size];
    controllerFiles = new File[size];
    return 1;
  }
  
  public File[] concatFileArray(File[] modelFiles, File[] controllerFiles) {
    int resultSize = this.modelFiles.length + this.controllerFiles.length;
    File[] result = new File[resultSize];
    int i = 0; for (int j = 0; i < resultSize; j++) {
      result[i] = modelFiles[j];
      result[(i + 1)] = controllerFiles[j];i += 2;
    }
    
    return result;
  }
  
  public File[] getModelFiles() {
    return modelFiles;
  }
  
  public void setModelFiles(File[] modelFiles) {
    this.modelFiles = modelFiles;
  }
  
  public File[] getControllerFiles() {
    return controllerFiles;
  }
  
  public void setControllerFiles(File[] controllerFiles) {
    this.controllerFiles = controllerFiles;
  }
}
