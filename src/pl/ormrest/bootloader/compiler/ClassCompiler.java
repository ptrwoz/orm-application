package pl.ormrest.bootloader.compiler;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;


public class ClassCompiler
{
  private static URLClassLoader classLoader;
  
  public ClassCompiler()
  {
    classLoader = null;
  }
  
  public int compileFromFile(File[] files, File filesDirectory) throws IOException {
    JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
    try { StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);Throwable localThrowable3 = null;
      try { fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Arrays.asList(new File[] { filesDirectory }));
        Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(Arrays.asList(files));
        compiler.getTask(null, fileManager, null, null, null, compilationUnits).call();
        classLoader = URLClassLoader.newInstance(new URL[] { filesDirectory.toURL() });
        System.out.print(">>ORMAPP<< Success compile files.");
        return 1;
      }
      catch (Throwable localThrowable1)
      {
        localThrowable3 = localThrowable1;throw localThrowable1;


      }
      finally
      {

        if (fileManager != null) if (localThrowable3 != null) try { fileManager.close(); } catch (Throwable localThrowable2) { localThrowable3.addSuppressed(localThrowable2); } else { fileManager.close();
          }
      }
      return 0;
    }
    catch (Exception eCompileError)
    {
      System.out.print(">>ORMAPP<< Compile files error.");
      System.out.print(eCompileError);
    }
  }
  
  public Class<?> getClassByName(String name) throws ClassNotFoundException
  {
    return classLoader.loadClass(name);
  }
  
  public static URLClassLoader getClassLoader() {
    return classLoader;
  }
}
