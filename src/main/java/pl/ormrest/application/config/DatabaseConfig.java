package pl.ormrest.application.config;

/**
 * @author ptrwoz
 */
import pl.ormrest.bootloader.properties.ORMProperties;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Properties;
import javax.persistence.EntityManager;
import javax.persistence.SharedCacheMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.persistenceunit.MutablePersistenceUnitInfo;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitPostProcessor;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import pl.ormrest.bootloader.compiler.ClassCompiler;
import pl.ormrest.bootloader.generator.classes.ORMClass;
import pl.ormrest.bootloader.generator.classes.ORMClassManager;
import pl.ormrest.other.StringOperation;

@Primary
@Configuration
@Profile("application")
public class DatabaseConfig extends StringOperation {

    /**
     * @field
     */
    //@Autowired
    private LocalContainerEntityManagerFactoryBean entityManagerFactory = null;

    /**
     * @method
     */
    @Bean
    public DriverManagerDataSource dataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(ORMProperties.getoRMPropertiesObject().getDriverClassName());
        dataSource.setUrl(ORMProperties.getoRMPropertiesObject().getUrl());
        dataSource.setUsername(ORMProperties.getoRMPropertiesObject().getUsername());
        dataSource.setPassword(ORMProperties.getoRMPropertiesObject().getPassword());
        return dataSource;

    }

    /*public DriverManagerDataSource getDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(ORMProperties.getoRMPropertiesObject().getDriverClassName());
        dataSource.setUrl(ORMProperties.getoRMPropertiesObject().getUrl());
        dataSource.setUsername(ORMProperties.getoRMPropertiesObject().getUsername());
        dataSource.setPassword(ORMProperties.getoRMPropertiesObject().getPassword());
        return dataSource;
    }*/
    private PersistenceUnitPostProcessor getPersistenceUnitPostProcessor() {
        return new PersistenceUnitPostProcessor() {
            @Override
            public void postProcessPersistenceUnitInfo(MutablePersistenceUnitInfo mutablePersistenceUnitInfo) {
                List<ORMClass> ormModel = ORMClassManager.oRMClassModelList;
                for (ORMClass oRMClass : ormModel) {
                    mutablePersistenceUnitInfo.addManagedClassName(ORMProperties.getoRMPropertiesObject().getClassPackage() + "." + UppserFirstLetter(oRMClass.getName()));
                }
            }
        };
    }

    private Properties getJpaProperties() {
        Properties additionalProperties = new Properties();
        additionalProperties.put("hibernate.dialect", ORMProperties.getoRMPropertiesObject().getDialect());
        additionalProperties.put("hibernate.default_schema", ORMProperties.getoRMPropertiesObject().getSchema());
        additionalProperties.put("hibernate.jdbc", "TRACE");
        additionalProperties.put("hibernate.show_sql", "true");
        return additionalProperties; 
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws ClassNotFoundException, MalformedURLException, IOException {
        Thread.currentThread().setContextClassLoader(ClassCompiler.getClassLoader());
        this.entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
        this.entityManagerFactory.setDataSource(dataSource());
        this.entityManagerFactory.setPersistenceUnitPostProcessors(this.getPersistenceUnitPostProcessor());
        this.entityManagerFactory.setBeanClassLoader(ClassCompiler.getClassLoader());
        this.entityManagerFactory.setPackagesToScan("pl.ormrest.application.config");
        this.entityManagerFactory.setSharedCacheMode(SharedCacheMode.NONE);
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        this.entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
        this.entityManagerFactory.setJpaProperties(this.getJpaProperties());
        return this.entityManagerFactory;
    }

    @Bean
    public EntityManager entityManager() {
        return entityManagerFactory.getNativeEntityManagerFactory().createEntityManager();
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }
}
