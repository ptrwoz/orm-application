package pl.ormrest.application.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.ormrest.bootloader.generator.classes.ORMClass;
import pl.ormrest.bootloader.generator.classes.ORMClassManager;
import pl.ormrest.other.StringOperation;

/**
 *
 * @author ptrwoz
 */
@RestController
@Profile("application")
public class HomeController extends StringOperation{

    /**
     * @method
     */
    @RequestMapping(method = RequestMethod.GET, value = "")
    public @ResponseBody
    String home(HttpServletRequest request) {
        String logo = "=============[ ORM Application ]=============";
        String content = "<br/>";
        List<ORMClass> modelList = ORMClassManager.oRMClassModelList;
        String mainURl = request.getRequestURL().toString().replace(request.getRequestURI(), request.getContextPath());
        for (ORMClass oRMClass : modelList) {
            content += "==========[" + this.UppserFirstLetter(oRMClass.getName()) + "]==========<br/>";
            content += "=[save]= <a href=\"/" + this.UppserFirstLetter(oRMClass.getName()) + "/\">" + mainURl + "/" + this.UppserFirstLetter(oRMClass.getName()) + "</a> (PUT)<br/>";
            content += "=[edit]= <a href=\"/" + this.UppserFirstLetter(oRMClass.getName()) + "/\">" + mainURl + "/" + this.UppserFirstLetter(oRMClass.getName()) + "</a> (POST)<br/>";
            content += "=[delete]= <a href=\"/" + this.UppserFirstLetter(oRMClass.getName()) + "/\">" + mainURl + "/" + this.UppserFirstLetter(oRMClass.getName()) + "</a> (DELETE)<br/>";
            content += "=[getAll]= <a href=\"/" + this.UppserFirstLetter(oRMClass.getName()) + "/findAll/\">" + mainURl + "/" + this.UppserFirstLetter(oRMClass.getName()) + "/findAll</a>  (GET)<br/>";
            content += "=[getById]= <a href=\"/" + this.UppserFirstLetter(oRMClass.getName()) + "/1\">" + mainURl + "/" + this.UppserFirstLetter(oRMClass.getName()) + "/{id}</a> (GET)<br/>";
        }
        return logo + content;
    }
}
