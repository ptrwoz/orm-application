package pl.ormrest.main;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import pl.ormrest.bootloader.main.ORMSpringBoot;

/**
 *
 * @author ptrwoz
 */
@SpringBootApplication
@ComponentScan(basePackages = {"pl.ormrest.application.config", "pl.ormrest.application.controller", "pl.ormrest.bootloader.controller"})
public class Application {

    /**
     * @method
     */
    public static void main(String[] args) throws Exception {
        ORMSpringBoot oRMSpringBoot = new ORMSpringBoot(args, Application.class);
        oRMSpringBoot.run();
    }
};
