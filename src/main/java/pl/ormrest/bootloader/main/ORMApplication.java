package pl.ormrest.bootloader.main;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import pl.ormrest.bootloader.compiler.ClassCompiler;
import pl.ormrest.bootloader.generator.element.ControllerGenerator;
import pl.ormrest.bootloader.generator.DatabaseManager;
import pl.ormrest.bootloader.generator.FileManager;
import pl.ormrest.bootloader.generator.element.JavaFileGenerator;
import pl.ormrest.bootloader.generator.element.ModelGenerator;
import pl.ormrest.bootloader.generator.ORMGenerator;
import pl.ormrest.bootloader.generator.classes.ORMClass;
import pl.ormrest.bootloader.generator.classes.ORMClassManager;
import pl.ormrest.bootloader.properties.ORMProperties;
import pl.ormrest.bootloader.properties.ORMPropertiesObject;
import pl.ormrest.other.StringOperation;

/**
 *
 * @author ptrwoz
 */
public final class ORMApplication extends StringOperation {

    /**
     * @constructor
     */
    public ORMApplication() throws SQLException, IOException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, Exception {
        System.out.println(">>ORMAPP<< Start init ORM-APPLICATION.");
        this.oRMProperties = new ORMProperties();
        this.fileManager = new FileManager();
        this.databaseManager = new DatabaseManager();
        this.databaseManager.initDataSource();
        this.databaseManager.initDatabaseMetaData();
        if (this.databaseManager.testDataSource() == 1) {
            oRMClassManager = new ORMClassManager();
            fileManager.deleteOldGenerateFiles(new File(ORMProperties.getoRMPropertiesObject().getProjectPath()));
            ORMProperties.getoRMPropertiesObject().setClassPackage(ORMProperties.getoRMPropertiesObject().getClassPackage() + "." + this.databaseManager.getDataBaseName());
            JavaFileGenerator javaFileGenerator = new JavaFileGenerator(ORMProperties.getoRMPropertiesObject().getProjectPath(), databaseManager.getDataBaseName());
            ModelGenerator modelClass = new ModelGenerator();
            ControllerGenerator controllerClass = new ControllerGenerator();
            ClassCompiler classCompiler = new ClassCompiler();
            this.oRMGenerator = new ORMGenerator(classCompiler, javaFileGenerator, modelClass, controllerClass);
            this.oRMGenerator.getModelGenerator().initTableSchemes(databaseManager.getDatabaseMetaData());
            this.oRMGenerator.getControllerGenerator().initRepositoryClassCode(oRMGenerator.getModelGenerator().getClassNameList());
            this.generateJavaFiles();
            this.compileJavaFiles();
            this.initORMClassList();
            this.databaseManager.getDataSource().getConnection().close();
        }
    }

    public ORMApplication(ORMPropertiesObject oRMPropertiesObject) throws SQLException, IOException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, Exception {
        System.out.println(">>ORMAPP<< Start init ORM-APPLICATION.");
        this.oRMProperties = new ORMProperties(oRMPropertiesObject);
        this.fileManager = new FileManager();
        this.databaseManager = new DatabaseManager();
        this.databaseManager.initDataSource();
        this.databaseManager.initDatabaseMetaData();
        if (this.databaseManager.testDataSource() == 1) {
            this.oRMClassManager = new ORMClassManager();
            this.fileManager.deleteOldGenerateFiles(new File(ORMProperties.getoRMPropertiesObject().getProjectPath()));
            ORMProperties.getoRMPropertiesObject().setClassPackage(ORMProperties.getoRMPropertiesObject().getClassPackage() + "." + this.databaseManager.getDataBaseName());
            JavaFileGenerator javaFileGenerator = new JavaFileGenerator(ORMProperties.getoRMPropertiesObject().getProjectPath(), databaseManager.getDataBaseName());
            ModelGenerator modelClass = new ModelGenerator();
            ControllerGenerator controllerClass = new ControllerGenerator();
            ClassCompiler classCompiler = new ClassCompiler();
            this.oRMGenerator = new ORMGenerator(classCompiler, javaFileGenerator, modelClass, controllerClass);
            this.oRMGenerator.getModelGenerator().initTableSchemes(databaseManager.getDatabaseMetaData());
            this.oRMGenerator.getControllerGenerator().initRepositoryClassCode(oRMGenerator.getModelGenerator().getClassNameList());
            this.generateJavaFiles();
            this.compileJavaFiles();
            this.initORMClassList();
            this.databaseManager.getDataSource().getConnection().close();
        }
    }
    /**
     * @field
     */
    private ORMProperties oRMProperties = null;
    private ORMGenerator oRMGenerator = null;
    private FileManager fileManager = null;
    private DatabaseManager databaseManager = null;
    private ORMClassManager oRMClassManager = null;
    
    /**
     * @method
     */
    public int initORMClassList() throws ClassNotFoundException {
        for (int i = 0; i < ORMClassManager.oRMClassModelList.size(); i++) {
            ((ORMClass) ORMClassManager.oRMClassModelList.get(i)).setORMClass(oRMGenerator.getClassCompiler().getClassByName(ORMProperties.getoRMPropertiesObject().getClassPackage() + "." + UppserFirstLetter(((ORMClass) ORMClassManager.oRMClassModelList.get(i)).getName())));
            ((ORMClass) ORMClassManager.oRMClassControllerList.get(i)).setORMClass(oRMGenerator.getClassCompiler().getClassByName(ORMProperties.getoRMPropertiesObject().getClassPackage() + "." + UppserFirstLetter(new StringBuilder().append(((ORMClass) ORMClassManager.oRMClassModelList.get(i)).getName()).append("Controller").toString())));
        }
        return 1;
    }

    public int generateJavaFiles() {
        fileManager.initFiles(oRMGenerator.getModelGenerator().getClassCodeList().size());
        for (int i = 0; i < oRMGenerator.getModelGenerator().getClassCodeList().size(); i++) {
            File modelFile = oRMGenerator.getJavaFileGenerator().createFileFromString((String) oRMGenerator.getModelGenerator().getClassCodeList().get(i), (String) oRMGenerator.getModelGenerator().getClassNameList().get(i));
            ORMClassManager.oRMClassModelList.add(new ORMClass((String) oRMGenerator.getModelGenerator().getClassNameList().get(i)));
            File controllerFile = oRMGenerator.getJavaFileGenerator().createFileFromString((String) oRMGenerator.getControllerGenerator().getRepositoryCodeList().get(i), (String) oRMGenerator.getControllerGenerator().getRepositoryNameList().get(i));
            ORMClassManager.oRMClassControllerList.add(new ORMClass((String) oRMGenerator.getControllerGenerator().getRepositoryNameList().get(i)));
            this.fileManager.getModelFiles()[i] = modelFile;
            this.fileManager.getControllerFiles()[i] = controllerFile;
        }
        return 1;
    }

    private String getMainPath(String projectPath, String packageName) {
        int packageNamePartSize = packageName.split("\\.").length;
        String[] projectPathArray = projectPath.split("/");
        String result = "";
        for (int i = 0; i < projectPathArray.length - packageNamePartSize; i++) {
            result = result + projectPathArray[i] + "/";
        }
        return result;
    }

    private int compileJavaFiles() throws IOException, SQLException {
        String projectPath = ORMProperties.getoRMPropertiesObject().getProjectPath() + this.databaseManager.getDataBaseName();
        String packageName = ORMProperties.getoRMPropertiesObject().getClassPackage();
        String basePath = getMainPath(projectPath, packageName);
        File parentDirectory = new File(basePath);
        File[] allFiles = fileManager.concatFileArray(fileManager.getModelFiles(), fileManager.getControllerFiles());
        this.oRMGenerator.getClassCompiler().compileFromFile(allFiles, parentDirectory);
        return 1;
    }

}
