package pl.ormrest.bootloader.main;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 *
 * @author ptrwoz
 */
public class ORMSpringBoot {

    /**
     * @constructor
     */
    public ORMSpringBoot(String[] args, Class mainClass) {
        ORMSpringBoot.args = args;
        ORMSpringBoot.mainClass = mainClass;
        ORMSpringBoot.applicationContext = null;
    }
    /**
     * @field
     */
    public static String[] args = null;
    public static ConfigurableApplicationContext applicationContext = null;
    public static Class mainClass = null;

    /**
     * @method
     */
    public int run() {
        applicationContext = SpringApplication.run(mainClass, args);
        return 1;
    }
}
