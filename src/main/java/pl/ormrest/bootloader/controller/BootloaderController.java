package pl.ormrest.bootloader.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.ormrest.bootloader.properties.ORMPropertiesObject;
import pl.ormrest.bootloader.main.ORMApplication;
import pl.ormrest.bootloader.main.ORMSpringBoot;

@RestController
@Profile("bootloader")
public class BootloaderController {

    private ConfigurableApplicationContext applicationContext = null;

    @RequestMapping(method = RequestMethod.GET, value = "stop")
    public @ResponseBody
    String stopStandardApp() {
        if (applicationContext != null) {
            SpringApplication.exit(this.applicationContext, new ExitCodeGenerator() {
                @Override
                public int getExitCode() {
                    return 1;
                }
            });
            this.applicationContext = null;
            return "=============[ ORM Application ]=============<br/>Standard App stoped...";
        } else {
            return "=============[ ORM Application ]=============<br/>Standard App is already stop.";
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "start")
    public synchronized @ResponseBody
    String startStandardApp() throws SQLException, IOException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (this.applicationContext != null) {
            SpringApplication.exit(this.applicationContext, new ExitCodeGenerator() {
                @Override
                public int getExitCode() {
                    return 1;
                }
            });
        }
        this.applicationContext = null;
        try {
            ORMApplication oRMApplication = new ORMApplication();
            System.getProperties().put("server.port", 8081);
            System.getProperties().put("spring.profiles.active", "application");
            applicationContext = SpringApplication.run(ORMSpringBoot.mainClass, ORMSpringBoot.args);
            return "=============[ ORM Application ]=============<br/>Bootloader init run.";
        } catch (Exception e) {
            return "=============[ ORM Application ]=============<br/>Bootloader init error: " + e;
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value = "start")
    public synchronized @ResponseBody
    String startStandardApp(@RequestBody ORMPropertiesObject oRMPropertiesObject) throws SQLException, IOException, ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (this.applicationContext != null) {
            SpringApplication.exit(this.applicationContext, new ExitCodeGenerator() {
                @Override
                public int getExitCode() {
                    return 1;
                }
            });
        }
        this.applicationContext = null;
        try {
            ORMApplication oRMApplication = new ORMApplication(oRMPropertiesObject);
            System.getProperties().put("server.port", 8081);
            System.getProperties().put("spring.profiles.active", "application");
            applicationContext = SpringApplication.run(ORMSpringBoot.mainClass, ORMSpringBoot.args);
            return "=============[ ORM Application ]=============<br/>Bootloader init run.";
        } catch (Exception e) {
            return "=============[ ORM Application ]=============<br/>Bootloader init error: " + e;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "")
    public @ResponseBody
    String statusBootloader() {
        if (this.applicationContext != null) {
            return "=============[ ORM Application ]=============<br/>Bootloader server is running...<br/>Standard Application is running...";
        } else {
            return "=============[ ORM Application ]=============<br/>Bootloader server is running...<br/>Standard Application not work.";
        }
    }
}
