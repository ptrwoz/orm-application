package pl.ormrest.bootloader.compiler;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;

public class ClassCompiler {

    /**
     * @constructor
     */
    public ClassCompiler() {
        ClassCompiler.classLoader = null;
    }
    /**
     * @field
     */
    private static URLClassLoader classLoader;

    /**
     * @getter/setter
     */
    public static URLClassLoader getClassLoader() {
        return classLoader;
    }

    /**
     * @method
     */
    public int compileFromFile(File[] files, File outputDirectory) throws IOException {
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        try {
            StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
            fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Arrays.asList(new File[]{outputDirectory}));
            Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjectsFromFiles(Arrays.asList(files));
            compiler.getTask(null, fileManager, null, null, null, compilationUnits).call();
            classLoader = URLClassLoader.newInstance(new URL[]{outputDirectory.toURL()});
            System.out.print(">>ORMAPP<< Success compile files.");
            return 1;
        } catch (Exception e) {
            System.out.print(">>ORMAPP<< Compile files error. " + e);
            return 0;
        }
    }
    @JsonBackReference()
    public Class<?> getClassByName(String name) throws ClassNotFoundException {
        return classLoader.loadClass(name);
    }

}
