package pl.ormrest.bootloader.generator.element;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pl.ormrest.other.StringOperation;

/**
 *
 * @author ptrwoz
 */
public class JavaFileGenerator extends StringOperation {

    /**
     * field
     */
    private final String directPath;

    /**
     * @constructor
     */
    public JavaFileGenerator(String directPath, String databaseName) {
        this.directPath = directPath + databaseName;
        File theDir = new File(this.directPath);
        if (!theDir.exists()) {
            try {
                theDir.mkdir();
            } catch (SecurityException e) {
            }
        }
    }

    /**
     * @method
     */
    public File createFileFromString(String sourceCode, String fileName) {
        try {
            File file = new File(this.directPath);
            File sourceFile = File.createTempFile(this.UppserFirstLetter(fileName), ".java", file);
            sourceFile.deleteOnExit();
            try (FileWriter writer = new FileWriter(sourceFile)) {
                writer.write(sourceCode);
            }
            File editFile = new File(this.directPath + "/" + this.UppserFirstLetter(fileName) + ".java");
            sourceFile.renameTo(editFile);
            file.delete();
            return editFile;
        } catch (IOException ex) {
            Logger.getLogger(JavaFileGenerator.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

}
