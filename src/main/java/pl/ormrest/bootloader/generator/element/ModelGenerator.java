/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.ormrest.bootloader.generator.element;

import pl.ormrest.bootloader.generator.database.structure.FieldRelation;
import pl.ormrest.bootloader.generator.database.structure.FieldScheme;
import pl.ormrest.bootloader.generator.database.structure.TabelScheme;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import pl.ormrest.bootloader.properties.ORMProperties;

/**
 *
 * @author ptrwoz
 */
public class ModelGenerator {

    /**
     * @field
     */
    private List<FieldRelation> fieldRelations = null;
    private List<TabelScheme> tabelSchemeList = null;
    private List<String> classNameList = null;
    private List<String> classCodeList = null;

    /**
     * @getter/setter
     */
    public List<String> getClassNameList() {
        return classNameList;
    }

    public void setClassNameList(List<String> classNameList) {
        this.classNameList = classNameList;
    }

    public List<String> getClassCodeList() {
        return classCodeList;
    }

    public void setClassCodeList(List<String> classCodeList) {
        this.classCodeList = classCodeList;
    }

    /**
     * @method
     */
    private boolean isPrimaryKey(List<String> primaryKeys, String columnName) throws SQLException {
        for (String string : primaryKeys) {
            if (string.equals(columnName)) {
                return true;
            }
        }
        return false;
    }

    private boolean isNotNull(List<String> notNulls, String columnName) throws SQLException {
        for (String string : notNulls) {
            if (string.equals(columnName)) {
                return true;
            }
        }
        return false;
    }

    private boolean isUnique(List<String> notUniques, String columnName) throws SQLException {
        for (String string : notUniques) {
            if (string.equals(columnName)) {
                return true;
            }
        }
        return false;
    }

    private List<String> findPrimaryKeys(ResultSet resultPrimayKeys) throws SQLException {
        List<String> primaryKeyList = new ArrayList<>();
        while (resultPrimayKeys.next()) {
            primaryKeyList.add(resultPrimayKeys.getString(4));
        }
        return primaryKeyList;
    }

    private List<String> findNotNulls(ResultSet resultColumns) throws SQLException {
        List<String> notNullsList = new ArrayList<>();
        while (resultColumns.next()) {
            int nullable = resultColumns.getInt("NULLABLE");
            if (nullable != DatabaseMetaData.columnNullable) {
                notNullsList.add(resultColumns.getString(4));
            }
        }
        return notNullsList;
    }

    private List<String> findUniques(ResultSet resultUniques) throws SQLException {
        List<String> uniquesList = new ArrayList<>();
        while (resultUniques.next()) {
            boolean unique = resultUniques.getBoolean("NON_UNIQUE");
            if (!unique) {
                uniquesList.add(resultUniques.getString(3));
            }
        }
        return uniquesList;
    }

    private List<FieldRelation> getFieldRelationFromSourceTabel(String tabelName) {
        List<FieldRelation> tabelFieldRelationList = new ArrayList<>();
        for (FieldRelation fieldRelation : fieldRelations) {
            if (fieldRelation.getForeignKeySourceTabelName().equals(tabelName)) {
                tabelFieldRelationList.add(fieldRelation);
            }
        }
        return tabelFieldRelationList;
    }

    private List<FieldScheme> findFieldSchemes(String tabelName, ResultSet resultColumns, List<String> primaryKeyList, List<String> notNullList, List<String> uniqueList) throws SQLException, Exception {
        List<FieldScheme> fieldSchemes = new ArrayList<>();
        List<FieldRelation> tabelFieldRelationList = this.getFieldRelationFromSourceTabel(tabelName);
        boolean primaryKey = false;
        boolean notNull = false;
        boolean unique = false;
        while (resultColumns.next()) {
            primaryKey = false;
            notNull = false;
            unique = false;
            String columnType = resultColumns.getString("TYPE_NAME");
            String columnName = resultColumns.getString("COLUMN_NAME");
            FieldRelation currentRelation = null;
            for (FieldRelation fieldRelation : tabelFieldRelationList) {
                if (fieldRelation.getRelationType() != FieldRelation.Relations.OneToMany
                        && fieldRelation.getForeignKeySourceFieldName().equals(columnName)) {
                    currentRelation = fieldRelation;
                }
            }

            if (this.isPrimaryKey(primaryKeyList, columnName)) {
                primaryKey = true;
            }
            if (this.isNotNull(notNullList, columnName)) {
                notNull = true;
            }
            if (this.isUnique(uniqueList, columnName)) {
                unique = true;
            }
            fieldSchemes.add(new FieldScheme(columnType, columnName, primaryKey, notNull, unique, currentRelation));
        }
        for (FieldRelation fieldRelation : tabelFieldRelationList) {
            if (fieldRelation.getRelationType() == FieldRelation.Relations.OneToMany) {
                fieldSchemes.add(new FieldScheme(fieldRelation.getForeignKeyDestinyTabelName(), fieldRelation.getForeignKeySourceFieldName(), false, notNull, unique, fieldRelation));
            }
        }
        return fieldSchemes;
    }

    private int initRelationsWithoutType(DatabaseMetaData databaseMetaData, ResultSet tabelsData) throws SQLException {
        this.fieldRelations = new ArrayList<>();
        while (tabelsData.next()) {
            ResultSet foreignKeys = databaseMetaData.getImportedKeys("", "", tabelsData.getString(3));
            while (foreignKeys.next()) {
                String sourceTableName = foreignKeys.getString("FKTABLE_NAME");
                String sourceColumnName = foreignKeys.getString("FKCOLUMN_NAME");
                String destinyTableName = foreignKeys.getString("PKTABLE_NAME");
                String destinyColumnName = foreignKeys.getString("PKCOLUMN_NAME");
                FieldRelation fieldRelation = new FieldRelation(sourceColumnName, sourceTableName, destinyColumnName, destinyTableName);
                this.fieldRelations.add(fieldRelation);
            }
        }
        return 1;
    }

    /*private boolean isIndex(DatabaseMetaData databaseMetaData, String tabel, String column) throws SQLException {
        ResultSet index = databaseMetaData.getIndexInfo("", "", tabel, true, true);
        while (index.next()) {
            if (!index.getString("NON_UNIQUE").equals(column) && index.getString("COLUMN_NAME").equals(column)) {
                return true;
            }
        }
        return false;
    }*/
    private int initRelationsWithType(DatabaseMetaData databaseMetaData) throws SQLException {
        List<FieldRelation> newFieldRelations = new ArrayList<>();
        for (int i = 0; i < this.fieldRelations.size(); i++) {
            for (int j = 0; j < this.fieldRelations.size(); j++) {
                if (this.fieldRelations.get(i).getRelationType() == FieldRelation.Relations.None) {
                    ResultSet foreignKeys = databaseMetaData.getImportedKeys("", "", this.fieldRelations.get(i).getForeignKeyDestinyTabelName());
                    boolean oneToOneFlag = false;
                    while (foreignKeys.next()) {
                        String sourceTableName = foreignKeys.getString("FKTABLE_NAME");
                        String sourceColumnName = foreignKeys.getString("FKCOLUMN_NAME");
                        String destinyTableName = foreignKeys.getString("PKTABLE_NAME");
                        String destinyColumnName = foreignKeys.getString("PKCOLUMN_NAME");
                        /*if (destinyTableName.equals(this.fieldRelations.get(i).getForeignKeySourceTabelName())) {
                            boolean unique1 = this.isIndex(databaseMetaData, sourceTableName, sourceColumnName);
                            boolean unique2 = this.isIndex(databaseMetaData, this.fieldRelations.get(j).getForeignKeySourceTabelName(), this.fieldRelations.get(j).getForeignKeySourceFieldName());
                            if (unique1 && unique2) {
                                this.fieldRelations.get(i).setRelationType(FieldRelation.Relations.OneToOne);
                                if (this.fieldRelations.get(j).getRelationType() == FieldRelation.Relations.None) {
                                    this.fieldRelations.get(j).setRelationType(FieldRelation.Relations.OneToOne);
                                }
                                oneToOneFlag = true;
                                break;
                            }
                        }*/
                    }
                    //if (oneToOneFlag == false) {
                    ResultSet resultPrimayKeys = databaseMetaData.getPrimaryKeys("", "", this.fieldRelations.get(i).getForeignKeySourceTabelName());
                    List<String> primaryKays = new ArrayList<>();
                    primaryKays = this.findPrimaryKeys(resultPrimayKeys);
                    this.fieldRelations.get(i).setRelationType(FieldRelation.Relations.ManyToOne);
                    newFieldRelations.add(new FieldRelation(
                            primaryKays.get(0),
                            this.fieldRelations.get(i).getForeignKeyDestinyTabelName(),
                            this.fieldRelations.get(i).getForeignKeySourceFieldName(),
                            this.fieldRelations.get(i).getForeignKeySourceTabelName(),
                            FieldRelation.Relations.OneToMany));
                    //}
                } else {
                    break;
                }
            }
        }
        for (FieldRelation newFieldRelation : newFieldRelations) {
            this.fieldRelations.add(newFieldRelation);
        }
        return 1;
    }

    public int initTableSchemes(DatabaseMetaData databaseMetaData) throws SQLException, Exception {
        tabelSchemeList = new ArrayList<>();
        classCodeList = new ArrayList<>();
        classNameList = new ArrayList<>();
        ResultSet tabelsData = databaseMetaData.getTables(null, ORMProperties.getoRMPropertiesObject().getSchema(), "%", null);
        this.initRelationsWithoutType(databaseMetaData, tabelsData);
        this.initRelationsWithType(databaseMetaData);
        tabelsData = databaseMetaData.getTables(null, ORMProperties.getoRMPropertiesObject().getSchema(), "%", null);
        while (tabelsData.next()) {

            ResultSet resultColumns = databaseMetaData.getColumns("", "", tabelsData.getString(3), "");
            ResultSet resultPrimayKeys = databaseMetaData.getPrimaryKeys("", "", tabelsData.getString(3));
            ResultSet resultUniques = databaseMetaData.getIndexInfo("", "", tabelsData.getString(3), true, true);

            List<String> primaryKeyList = this.findPrimaryKeys(resultPrimayKeys);
            List<String> notNullList = this.findNotNulls(resultColumns);
            List<String> uniqueList = this.findUniques(resultUniques);

            resultColumns = databaseMetaData.getColumns("", "", tabelsData.getString(3), "");
            List<FieldScheme> fieldSchemes = this.findFieldSchemes(tabelsData.getString(3), resultColumns, primaryKeyList, notNullList, uniqueList);
            if (fieldSchemes.isEmpty() || primaryKeyList.isEmpty()) {
                continue;
            }
            TabelScheme tabelScheme = new TabelScheme(tabelsData.getString(3), fieldSchemes);
            this.tabelSchemeList.add(tabelScheme);
        }

        for (TabelScheme tabelScheme : tabelSchemeList) {
            String classCode = tabelScheme.generateTabelCode(ORMProperties.getoRMPropertiesObject().getClassPackage());
            classCodeList.add(classCode);
            classNameList.add(tabelScheme.getTabelSchemeName());
        }
        return 1;
    }

    public String getDataBaseName(DatabaseMetaData databaseMetaData) throws SQLException {
        return databaseMetaData.getURL().split("/")[databaseMetaData.getURL().split("/").length - 1];
    }
}
