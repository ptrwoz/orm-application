package pl.ormrest.bootloader.generator.element;

import java.util.ArrayList;
import java.util.List;
import pl.ormrest.bootloader.properties.ORMProperties;
import pl.ormrest.other.StringOperation;

/**
 *
 * @author ptrwoz
 */
public class ControllerGenerator extends StringOperation {

    /**
     * @field
     */
    private List<String> controllerNameList = null;
    private List<String> controllerCodeList = null;

    /**
     * @getter/setter
     */
    public void setRepositoryNameList(List<String> repositoryNameList) {
        this.controllerNameList = repositoryNameList;
    }

    public void setRepositoryCodeList(List<String> repositoryCodeList) {
        this.controllerCodeList = repositoryCodeList;
    }

    public List<String> getRepositoryCodeList() {
        return controllerCodeList;
    }

    public List<String> getRepositoryNameList() {
        return controllerNameList;
    }

    /**
     * @method
     */
    public int initRepositoryClassCode(List<String> className) {
        this.controllerNameList = new ArrayList<>();
        this.controllerCodeList = new ArrayList<>();
        for (int i = 0; i < className.size(); i++) {
            this.controllerNameList.add(className.get(i) + "Controller");
            String packageCode = this.initPackage(ORMProperties.getoRMPropertiesObject().getClassPackage());
            String importLibraryCode = this.initImportLibrary();
            String interfaceAnnotationCode = "@RestController ";
            String interfaceBodyCode = "public class " + this.UppserFirstLetter(className.get(i)) + "Controller extends ORMRepository { "
                    + this.initConstructorMethod(className.get(i))
                    + this.initCreateMethod(className.get(i))
                    + this.initUpdateMethod(className.get(i))
                    + this.initDeleteMethod(className.get(i))
                    + this.initFindAllMethod(className.get(i))
                    + this.initFindById(className.get(i)) + "}";
            this.controllerCodeList.add((packageCode + importLibraryCode + interfaceAnnotationCode + interfaceBodyCode));
        }
        return 1;
    }

    private String initPackage(String packageName) {
        return "package " + packageName + ";";
    }

    private String initImportLibrary() {
        return "import org.springframework.web.bind.annotation.RequestMapping;"
                + "import org.springframework.web.bind.annotation.RequestMethod;"
                + "import org.springframework.web.bind.annotation.ResponseBody;"
                + "import org.springframework.web.bind.annotation.RestController;"
                + "import org.springframework.web.bind.annotation.RequestBody;"
                + "import org.springframework.web.bind.annotation.PathVariable;"
                + "import pl.ormrest.bootloader.generator.classes.ORMRepository;"
                + "import java.util.List;";
    }

    private String initConstructorMethod(String name) {
        return "public " + this.UppserFirstLetter(name) + "Controller() { super(" + this.UppserFirstLetter(name) + ".class ); }";
    }

    private String initCreateMethod(String name) {
        return "@RequestMapping(method = RequestMethod.PUT, value = \"/" + this.UppserFirstLetter(name) + "/\")"
                + "public @ResponseBody String save" + this.UppserFirstLetter(name) + "(@RequestBody " + this.UppserFirstLetter(name) + " e) { save(e); return \"1\";}";
    }

    private String initUpdateMethod(String name) {

        return "@RequestMapping(method = RequestMethod.POST, value = \"/" + this.UppserFirstLetter(name) + "/\")"
                + "public @ResponseBody String update" + this.UppserFirstLetter(name) + "(@RequestBody " + this.UppserFirstLetter(name) + " e) { update(e); return \"1\";}";
    }

    private String initDeleteMethod(String name) {

        return "@RequestMapping(method = RequestMethod.DELETE, value = \"/" + this.UppserFirstLetter(name) + "/\")"
                + "public @ResponseBody String delete" + this.UppserFirstLetter(name) + "(@RequestBody " + this.UppserFirstLetter(name) + " e) { delete(e); return \"1\";}";
    }

    private String initFindAllMethod(String name) {
        return "@RequestMapping(method = RequestMethod.GET, value = \"/" + this.UppserFirstLetter(name) + "/findAll\")"
                + "public @ResponseBody List<Object> findAll" + this.UppserFirstLetter(name) + "() {"
                + "return  findAll();}";
    }

    private String initFindById(String name) {
        return "@RequestMapping(method = RequestMethod.GET, value = \"/" + this.UppserFirstLetter(name) + "/{id}\")"
                + "public @ResponseBody Object findById" + this.UppserFirstLetter(name) + "(@PathVariable(value=\"id\") int id) {"
                + "return  findById(id);}";
    }

}
