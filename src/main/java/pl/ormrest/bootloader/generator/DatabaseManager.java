package pl.ormrest.bootloader.generator;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import pl.ormrest.bootloader.properties.ORMProperties;

/**
 *
 * @author ptrwoz
 */
public class DatabaseManager {

    /**
     * @constructor
     */
    public DatabaseManager() {
        this.dataSource = new DriverManagerDataSource();
    }
    /**
     * @field
     */
    private DriverManagerDataSource dataSource = null;
    private DatabaseMetaData databaseMetaData = null;

    /**
     * @getter/setter
     */
    public DriverManagerDataSource getDataSource() {
        return dataSource;
    }

    public DatabaseMetaData getDatabaseMetaData() {
        return databaseMetaData;
    }
    /**
     * @method
     */
    public String getDataBaseName() throws SQLException {
        return databaseMetaData.getURL().split("/")[databaseMetaData.getURL().split("/").length - 1];
    }

    public int initDatabaseMetaData() throws SQLException {
        if (this.dataSource != null) {
            this.databaseMetaData = this.dataSource.getConnection().getMetaData();
            return 1;
        } else {
            return 0;
        }
    }

    public int testDataSource() throws SQLException {
        if (!this.dataSource.getConnection().isClosed() || dataSource.getConnection() != null) {
            System.out.println(">>ORMAPP<< Success connection data base.");
            return 1;
        } else {
            System.out.println(">>ORMAPP<< Error connection data base.");
            return 0;
        }
    }

    public int initDataSource() throws SQLException {
        try {
            if (this.dataSource != null) {
                this.dataSource = new DriverManagerDataSource();
                this.dataSource.setDriverClassName(ORMProperties.getoRMPropertiesObject().getDriverClassName());
                this.dataSource.setUrl(ORMProperties.getoRMPropertiesObject().getUrl());
                this.dataSource.setUsername(ORMProperties.getoRMPropertiesObject().getUsername());
                this.dataSource.setPassword(ORMProperties.getoRMPropertiesObject().getPassword());
                return 1;
            } else {
                return 0;
            }
        } catch (Exception eErrorProperties) {
            System.out.println(">>ORMAPP<< Error properties in ormconfig.properties.");
            System.out.println(eErrorProperties);
            return 0;
        }
    }
}
