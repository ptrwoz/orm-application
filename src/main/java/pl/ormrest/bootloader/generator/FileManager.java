package pl.ormrest.bootloader.generator;

import java.io.File;
import pl.ormrest.bootloader.properties.ORMProperties;

/**
 *
 * @author ptrwoz
 */
public class FileManager {

    /**
     * @field
     */
    private File[] modelFiles = null;
    private File[] controllerFiles = null;

    /**
     * @getter/setter
     */
    public File[] getModelFiles() {
        return modelFiles;
    }

    public void setModelFiles(File[] modelFiles) {
        this.modelFiles = modelFiles;
    }

    public File[] getControllerFiles() {
        return controllerFiles;
    }

    public void setControllerFiles(File[] controllerFiles) {
        this.controllerFiles = controllerFiles;
    }

    /**
     * @method
     */
    public void deleteOldGenerateFiles(File directory) {
        File[] folders = directory.listFiles();
        try {
            for (File folder : folders) {
                File[] files = folder.listFiles();
                for (File file : files) {
                    file.delete();
                }
                folder.delete();
            }
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }

    }

    public int initFiles(int size) {
        this.modelFiles = new File[size];
        this.controllerFiles = new File[size];
        return 1;
    }

    public File[] concatFileArray(File[] modelFiles, File[] controllerFiles) {
        int resultSize = this.modelFiles.length + this.controllerFiles.length;
        File[] result = new File[resultSize];
        for (int i = 0, j = 0; i < resultSize; i += 2, j++) {
            result[i] = modelFiles[j];
            result[i + 1] = controllerFiles[j];
        }
        return result;
    }

}
