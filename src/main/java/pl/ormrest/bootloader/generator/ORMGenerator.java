package pl.ormrest.bootloader.generator;

import pl.ormrest.bootloader.generator.element.JavaFileGenerator;
import pl.ormrest.bootloader.generator.element.ControllerGenerator;
import pl.ormrest.bootloader.generator.element.ModelGenerator;
import pl.ormrest.bootloader.compiler.ClassCompiler;
/**
 * 
 * @author ptrwoz
 */
public class ORMGenerator {

    /**
     * @constructor
     */
    public ORMGenerator(ClassCompiler classCompiler, JavaFileGenerator javaFileGenerator, ModelGenerator modelGenerator, ControllerGenerator controllerGenerator) {
        this.classCompiler = classCompiler;
        this.javaFileGenerator = javaFileGenerator;
        this.modelGenerator = modelGenerator;
        this.controllerGenerator = controllerGenerator;
    }
    /**
     * @field
     */
    private ClassCompiler classCompiler = null;
    private JavaFileGenerator javaFileGenerator = null;
    private ModelGenerator modelGenerator = null;
    private ControllerGenerator controllerGenerator = null;

    /**
     * @getter/setter 
     */
    public JavaFileGenerator getJavaFileGenerator() {
        return javaFileGenerator;
    }

    public ModelGenerator getModelGenerator() {
        return modelGenerator;
    }

    public ControllerGenerator getControllerGenerator() {
        return controllerGenerator;
    }

    public ClassCompiler getClassCompiler() {
        return classCompiler;
    }
}
