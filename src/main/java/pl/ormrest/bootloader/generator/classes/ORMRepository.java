package pl.ormrest.bootloader.generator.classes;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ptrwoz
 */
public class ORMRepository {

    /**
     * @constructor
     */
    public ORMRepository(Class entityClass) {
        this.entityClass = entityClass;
    }
    /**
     * field
     */
    private final Class entityClass;
    @Autowired
    private EntityManager entityManager;

    /**
     * @getter/setter
     */
    protected EntityManager getSession() {
        return this.entityManager;
    }

    /**
     * @method
     */
    public void save(Object entity) {
        EntityTransaction tr = null;
        try {
            tr = entityManager.getTransaction();
            tr.begin();
            entityManager.persist(entity);
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

    }

    public void delete(Object entity) {
        EntityTransaction tr = null;
        try {
            tr = entityManager.getTransaction();
            tr.begin();
            Object f = entityManager.merge(entity);
            entityManager.remove(f);
            tr.commit();
        } catch (Exception e) {
            tr.rollback();
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    public void update(Object entity) {
        EntityTransaction tr = null;
        try {
            tr = entityManager.getTransaction();
            tr.begin();
            entityManager.merge(entity);
            tr.commit();
        } catch (Exception e) {
            e.printStackTrace();
            tr.rollback();
            throw new RuntimeException(e.getMessage());
        }
    }
 
    public List<Object> findAll() {

        Query query = entityManager.createQuery("SELECT e FROM " + entityClass.getSimpleName() + " e");
        return query.getResultList();
    }

    public Object findById(int id) {
        return entityManager.find(entityClass, id);
    }

}
