package pl.ormrest.bootloader.generator.classes;

import java.util.ArrayList;
import java.util.List;

public class ORMClassManager {

    /**
     * @constructor
     */
    public ORMClassManager() {
        oRMClassModelList = new ArrayList();
        oRMClassControllerList = new ArrayList();
    }
    /**
     * @field
     */
    public static List<ORMClass> oRMClassModelList;
    public static List<ORMClass> oRMClassControllerList;

}
