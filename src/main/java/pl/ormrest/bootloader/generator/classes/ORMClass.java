package pl.ormrest.bootloader.generator.classes;


/**
 *
 * @author ptrwoz
 */

public class ORMClass {

    /**
     * @constructor
     */
    public ORMClass(String name) {
        this.name = name;
    }
    /**
     * @field
     */
    private String name;
    private Class<?> ORMClass;

    /**
     * @getter/setter
     */
    public String getName() {
        return name;
    }

    public Class<?> getORMClass() {
        return ORMClass;
    }

    public void setORMClass(Class<?> ORMClass) {
        this.ORMClass = ORMClass;
    }

}
