package pl.ormrest.bootloader.generator.database.structure;

import pl.ormrest.other.StringOperation;

/**
 *
 * @author ptrwoz
 */
public class FieldRelation extends StringOperation {

    /**
     * @constructor
     */
    public FieldRelation(String foreignKeySourceFieldName, String foreignKeySourceTabelName, String foreignKeyDestinyFieldName, String foreignKeyDestinyTabelName, Relations relationType) {
        this.foreignKeySourceFieldName = foreignKeySourceFieldName;
        this.foreignKeySourceTabelName = foreignKeySourceTabelName;
        this.foreignKeyDestinyFieldName = foreignKeyDestinyFieldName;
        this.foreignKeyDestinyTabelName = foreignKeyDestinyTabelName;
        this.relationType = relationType;
    }

    public FieldRelation(String foreignKeySourceFieldName, String foreignKeySourceTabelName, String foreignKeyDestinyFieldName, String foreignKeyDestinyTabelName) {
        this.foreignKeySourceFieldName = foreignKeySourceFieldName;
        this.foreignKeySourceTabelName = foreignKeySourceTabelName;
        this.foreignKeyDestinyFieldName = foreignKeyDestinyFieldName;
        this.foreignKeyDestinyTabelName = foreignKeyDestinyTabelName;
        this.relationType = Relations.None;
    }
    /**
     * @field
     */
    private String foreignKeySourceFieldName;
    private String foreignKeySourceTabelName;
    private String foreignKeyDestinyFieldName;
    private String foreignKeyDestinyTabelName;
    private Relations relationType;

    /**
     * @getter/setter
     */
    public void setForeignKeySourceFieldName(String foreignKeySourceFieldName) {
        this.foreignKeySourceFieldName = foreignKeySourceFieldName;
    }

    public void setForeignKeySourceTabelName(String foreignKeySourceTabelName) {
        this.foreignKeySourceTabelName = foreignKeySourceTabelName;
    }

    public void setForeignKeyDestinyFieldName(String foreignKeyDestinyFieldName) {
        this.foreignKeyDestinyFieldName = foreignKeyDestinyFieldName;
    }

    public void setForeignKeyDestinyTabelName(String foreignKeyDestinyTabelName) {
        this.foreignKeyDestinyTabelName = foreignKeyDestinyTabelName;
    }

    public String getForeignKeySourceFieldName() {
        return foreignKeySourceFieldName;
    }

    public String getForeignKeySourceTabelName() {
        return foreignKeySourceTabelName;
    }

    public String getForeignKeyDestinyFieldName() {
        return foreignKeyDestinyFieldName;
    }

    public String getForeignKeyDestinyTabelName() {
        return foreignKeyDestinyTabelName;
    }

    public Relations getRelationType() {
        return relationType;
    }

    public void setRelationType(Relations relationType) {
        this.relationType = relationType;
    }

    /**
     *  @method
     */
    public enum Relations {
        OneToMany, ManyToOne, OneToOne, None
    }

    public String getType() {
        if (this.relationType == Relations.OneToMany) {
            return "Set<" + this.UppserFirstLetter(this.foreignKeyDestinyTabelName) + ">";
        } else {
            return this.UppserFirstLetter(this.foreignKeyDestinyTabelName);
        }
    }

    public String getName() {
        if (this.relationType == Relations.OneToMany) {
            return this.foreignKeyDestinyTabelName + "Set";
        } else {
            return this.foreignKeySourceFieldName;
        }
    }

    public String getFieldRelationCode() {
        if (this.relationType == Relations.OneToMany) {
            return "Set<" + this.UppserFirstLetter(this.foreignKeyDestinyTabelName) + "> " + this.foreignKeyDestinyTabelName + "Set;";
        } else if (this.relationType == Relations.ManyToOne) {
            return this.UppserFirstLetter(this.foreignKeyDestinyTabelName) + " " + this.foreignKeySourceFieldName + ";";
        } else {
            return this.UppserFirstLetter(this.foreignKeyDestinyTabelName) + " " + this.foreignKeySourceFieldName + ";";
        }
    }

    public String getAnnotationCode() {

        if (this.relationType == Relations.OneToMany) {
            return " @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = \"id\") @JsonIdentityReference(alwaysAsId=true) @OneToMany( mappedBy = \"" + this.foreignKeyDestinyFieldName + "\", fetch = FetchType.EAGER, targetEntity =" + this.UppserFirstLetter(this.foreignKeyDestinyTabelName) + ".class )";
        } else if (this.relationType == Relations.ManyToOne) {
            return " @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = \"id\") @JsonIdentityReference(alwaysAsId=true) @JoinColumn(name = \"" + this.foreignKeySourceFieldName + "\", referencedColumnName = \"" + this.foreignKeyDestinyFieldName + "\")"
                    + "@ManyToOne(fetch = FetchType.EAGER) ";
        } else {
            return " @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = \"id\") @JsonIdentityReference(alwaysAsId=true) @JoinColumn(name = \"" + this.foreignKeyDestinyFieldName + "\", referencedColumnName = \"" + this.foreignKeyDestinyFieldName + "\")"
                    + "@OneToOne(fetch = FetchType.EAGER)";
        }
    }

}
