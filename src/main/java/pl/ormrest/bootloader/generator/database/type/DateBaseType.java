/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.ormrest.bootloader.generator.database.type;

/**
 *
 * @author ptrwoz
 */
public interface DateBaseType {
    public Class<?> typeRecognition(String type);
}
