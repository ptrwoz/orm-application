package pl.ormrest.bootloader.generator.database.structure;

import java.sql.SQLException;
import java.util.List;
import pl.ormrest.other.StringOperation;

/**
 *
 * @author ptrwoz
 */
public class TabelScheme extends StringOperation {

    /**
     * @constructor
     */
    public TabelScheme(String name, List<FieldScheme> columns) throws SQLException {
        this.name = name;
        this.fieldSchemeList = columns;
        System.out.println(">>ORMAPP<< TabelScheme create: " + this.name);
    }
    /**
     * @field
     */
    private String name = null;
    private List<FieldScheme> fieldSchemeList = null;
    private final String IMPORT_LIBRARY_CODE = "import java.io.Serializable;"
            + "import javax.persistence.Column;"
            + "import javax.persistence.JoinColumn;"
            + "import javax.persistence.GenerationType;"
            + "import javax.persistence.Entity;"
            + "import javax.persistence.GeneratedValue;"
            + "import java.util.Set;"
            + "import javax.persistence.Id;"
            + "import javax.persistence.FetchType;"
            + "import javax.persistence.ManyToOne;"
            + "import javax.persistence.Table;"
            + "import javax.persistence.OneToOne;"
            + "import javax.persistence.OneToMany;"
            + "import com.fasterxml.jackson.annotation.JsonIdentityInfo;"
            + "import com.fasterxml.jackson.annotation.ObjectIdGenerators;"
            + "import com.fasterxml.jackson.annotation.JsonBackReference;"
            + "import com.fasterxml.jackson.annotation.JsonManagedReference;"
            + "import com.fasterxml.jackson.annotation.JsonIgnore;"
            + "import com.fasterxml.jackson.annotation.JsonIdentityReference;"
            + "import com.voodoodyne.jackson.jsog.JSOGGenerator;"
            + "import javax.persistence.CascadeType;";

    /**
     * @getter/setter
     */
    public FieldScheme getFieldSchemeByName(String name) {
        for (FieldScheme fieldScheme : fieldSchemeList) {
            if (name.equals(fieldScheme.getName())) {
                return fieldScheme;
            }
        }
        return null;
    }

    public String getTabelSchemeName() {
        return name;
    }

    public List<FieldScheme> getFieldSchemeList() {
        return fieldSchemeList;
    }

    public void setFieldSchemeList(List<FieldScheme> fieldSchemeList) {
        this.fieldSchemeList = fieldSchemeList;
    }

    private String generateClassBodyCode() {
        String constructorCode = "public " + this.UppserFirstLetter(this.name) + "(";
        String constructorBodyCode = "{";
        String fieldsCode = "";
        String emptyConstructor = "public " + this.UppserFirstLetter(this.name) + "(){}";
        for (FieldScheme fieldScheme : fieldSchemeList) {
            if (fieldSchemeList.get(fieldSchemeList.size() - 1).equals(fieldScheme)) {
                constructorCode += fieldScheme.getJavaTypeAndName() + ")";
            } else {
                constructorCode += fieldScheme.getJavaTypeAndName() + ",";
            }
            constructorBodyCode += "this." + fieldScheme.getName() + " = " + fieldScheme.getName() + ";";
            fieldsCode += fieldScheme.getFieldAnnotationsCode();
            fieldsCode += "private " + fieldScheme.getFieldCode();
            fieldsCode += fieldScheme.getGetterFieldCode();
            fieldsCode += fieldScheme.getSetterFieldCode();
        }
        constructorBodyCode += "}";
        constructorCode += constructorBodyCode;
        return fieldsCode + constructorCode + emptyConstructor;
    }

    public String generateTabelCode(String packageCode) throws SQLException {
        String tabelPackageCode = "package " + packageCode + ";";
        String importLibraryCode = this.IMPORT_LIBRARY_CODE;
        String classAnnotationsCode = "@Entity @Table(name = \"" + this.name + "\")";
        String classCode = "public class " + this.UppserFirstLetter(this.name) + " implements Serializable {";
        classCode += this.generateClassBodyCode() + "}";
        classCode = tabelPackageCode + importLibraryCode + classAnnotationsCode + classCode;
        return classCode;
    }

}
