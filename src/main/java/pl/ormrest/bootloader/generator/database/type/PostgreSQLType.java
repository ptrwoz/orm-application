package pl.ormrest.bootloader.generator.database.type;

/**
 *
 * @author ptrwoz
 */
public class PostgreSQLType implements DateBaseType {

    @Override
    public Class<?> typeRecognition(String type) {
        switch (type) {

            case "text":
                return String.class;
            case "int4":
                return int.class;
            case "int8":
                return int.class;
            case "bigserial":
                return int.class;
            case "bytea":
                return byte[].class;
            case "char":
                return byte[].class;
            case "bit":
                return byte[].class;
            case "bool":
                return boolean.class;
            case "numeric":
                return double.class;
            case "float4":
                return double.class;
            case "timetz":
                return java.sql.Date.class;
            default:
                return null;
        }
    }

}
