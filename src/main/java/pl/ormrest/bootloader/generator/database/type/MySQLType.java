package pl.ormrest.bootloader.generator.database.type;

/**
 *
 * @author ptrwoz
 */
public class MySQLType implements DateBaseType{

    public Class<?> typeRecognition(String type) {
        switch (type) {
            case "CHAR":
                return String.class;
            case "VARCHAR":
                return String.class;
            case "TINYTEXT":
                return String.class;
            case "TEXT":
                return String.class;
            case "MEDIUMTEXT":
                return String.class;
            case "LONGTEXT":
                return String.class;
            case "TINYBLOB":
                return byte[].class;
            case "BLOB":
                return byte[].class;
            case "MEDIUMBLOB":
                return byte[].class;
            case "LONGBLOB":
                return byte[].class;
            case "SET":
                return String.class;
            case "ENUM":
                return String.class;
            case "DATE":
                return java.sql.Date.class;
            case "TIME":
                return java.sql.Time.class;
            case "DATETIME":
                return java.sql.Timestamp.class;
            case "YEAR":
                return java.sql.Date.class;
            case "TIMESTAMP":
                return String.class;
            case "TINYINT":
                return byte.class;
            case "SMALLINT":
                return short.class;
            case "MEDIUMINT":
                return int.class;
            case "INT":
                return int.class;
            case "BIGINT":
                return long.class;
            case "FLOAT":
                return float.class;
            case "DOUBLE":
                return double.class;
            case "DECIMAL":
                return java.math.BigDecimal.class;
            case "BIT":
                return byte[].class;
            case "BINARY":
                return byte[].class;
            case "VARBINARY":
                return byte[].class;
            default:
                return null;
        }
    }
}
