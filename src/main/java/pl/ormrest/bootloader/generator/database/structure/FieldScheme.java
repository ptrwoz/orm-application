package pl.ormrest.bootloader.generator.database.structure;

import pl.ormrest.bootloader.generator.database.type.MySQLType;
import pl.ormrest.bootloader.generator.database.type.PostgreSQLType;
import pl.ormrest.bootloader.properties.ORMProperties;
import pl.ormrest.other.StringOperation;

/**
 *
 * @author ptrwoz
 */
public class FieldScheme extends StringOperation {

    /**
     * @constructor
     */
    public FieldScheme(String type, String name, boolean primaryKey, boolean notNull, boolean unique, FieldRelation fieldRelation) throws Exception {
        this.name = name;
        this.sqlType = type;
        this.primaryKey = primaryKey;
        this.notNull = notNull;
        this.unique = unique;
        this.javaType = this.typeRecognition(type);
        this.fieldRelation = fieldRelation;
        if (type == null) {
            System.out.print("FieldScheme: Type Error!");
        }
    }
    /**
     * @field
     */
    private final String name;
    private final String sqlType;
    private final String javaType;
    private final boolean primaryKey;
    private final boolean notNull;
    private final boolean unique;
    private FieldRelation fieldRelation;

    /**
     * @getter/setter
     */
    public void setFieldRelation(FieldRelation fieldRelation) {
        this.fieldRelation = fieldRelation;
    }

    public FieldRelation getFieldRelation() {
        return fieldRelation;
    }

    /**
     * @method
     */
    public String getJavaTypeAndName() {
        if (this.fieldRelation == null) {
            return this.javaType + " " + this.name;
        } else {
            return this.fieldRelation.getType() + " " + this.fieldRelation.getName();
        }
    }

    public String getName() {
        if (this.fieldRelation == null) {
            return this.name;
        } else {
            return this.fieldRelation.getName();
        }
    }

    public String getFieldCode() {
        if (this.fieldRelation != null) {
            return this.fieldRelation.getFieldRelationCode();
        } else {
            return this.javaType + " " + this.name + ";";
        }
    }

    public String getFieldAnnotationsCode() {
        if (this.primaryKey) {
            return " @Id @GeneratedValue(strategy = GenerationType.IDENTITY) @Column(name =\"" + this.name + "\", nullable = false, unique = true)";
        } else if (this.fieldRelation != null) {
            return this.fieldRelation.getAnnotationCode();
        } else if (this.notNull && this.unique) {
            return "@Column(name =\"" + this.name + "\", nullable = false, unique = true)";
        } else if (!this.notNull && this.unique) {
            return "@Column(name =\"" + this.name + "\", nullable = true, unique = true)";
        } else if (!this.notNull && !this.unique) {
            return "@Column(name =\"" + this.name + "\", nullable = true, unique = false)";
        } else {
            return "@Column(name =\"" + this.name + "\", nullable = false, unique = false)";
        }
    }

    public String getGetterFieldCode() {
        if (this.fieldRelation != null) {
            return "public " + this.fieldRelation.getType() + " get" + UppserFirstLetter(this.fieldRelation.getName()) + "(){return this." + this.fieldRelation.getName() + ";}";
        } else {
            return "public " + this.javaType + " get" + UppserFirstLetter(this.name) + "(){return this." + this.name + ";}";
        }
    }

    public String getSetterFieldCode() {
        if (this.fieldRelation != null) {
            return "public void" + " set" + UppserFirstLetter(this.fieldRelation.getName()) + "(" + this.fieldRelation.getType() + " value " + "){this." + this.fieldRelation.getName() + " = value;}";
        } else {
            return "public void" + " set" + UppserFirstLetter(this.name) + "(" + this.javaType + " value " + "){this." + this.name + " = value;}";
        }
    }

    public String typeRecognition(String sqlType) throws Exception {

        if (ORMProperties.getoRMPropertiesObject().getDriverClassName().equals("com.mysql.jdbc.Driver")) {
            MySQLType mySQLType = new MySQLType();
            Class<?> classType = mySQLType.typeRecognition(sqlType);
            if (classType != null) {
                return classType.getCanonicalName();
            } else {
                return null;
            }
        } else if (ORMProperties.getoRMPropertiesObject().getDriverClassName().equals("org.postgresql.Driver")) {
            PostgreSQLType postgreSQLType = new PostgreSQLType();
            Class<?> classType = postgreSQLType.typeRecognition(sqlType);
            if (classType != null) {
                return classType.getCanonicalName();
            } else {
                return null;
            }
        } else {
            throw new Exception("Don't found DriverClassName in type Rrecognition.");
        }

    }

}
