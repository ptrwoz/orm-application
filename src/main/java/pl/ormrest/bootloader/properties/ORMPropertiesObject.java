package pl.ormrest.bootloader.properties;

public class ORMPropertiesObject {

    /**
     * @constructor
     */
    public ORMPropertiesObject(String driverClassName, String url, String username, String password, String projectPath, String classPackage, String schema, String dialect) {
        this.driverClassName = driverClassName;
        this.url = url;
        this.username = username;
        this.password = password;
        this.projectPath = projectPath;
        this.classPackage = classPackage;
        this.schema = schema;
        this.dialect = dialect;
    }

    public ORMPropertiesObject(ORMPropertiesObject oRMPropertiesObject) {
        this.driverClassName = oRMPropertiesObject.driverClassName;
        this.url = oRMPropertiesObject.url;
        this.username = oRMPropertiesObject.username;
        this.password = oRMPropertiesObject.password;
        this.projectPath = oRMPropertiesObject.projectPath;
        this.classPackage = oRMPropertiesObject.classPackage;
        this.schema = oRMPropertiesObject.schema;
        this.dialect = oRMPropertiesObject.dialect;
    }

    ORMPropertiesObject() {
        driverClassName = null;
        url = null;
        username = null;
        password = null;
        projectPath = null;
        classPackage = null;
        schema = null;
        dialect = null;
    }
    /**
     * @field
     */
    private String driverClassName;
    private String url;
    private String username;
    private String password;
    private String projectPath;
    private String classPackage;
    private String schema;
    private String dialect;

    /**
     *  @getter/setter
     */
    public String getDriverClassName() {
        return driverClassName;
    }

    public void setDriverClassName(String driverClassName) {
        this.driverClassName = driverClassName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProjectPath() {
        return projectPath;
    }

    public void setProjectPath(String projectPath) {
        this.projectPath = projectPath;
    }

    public String getClassPackage() {
        return classPackage;
    }

    public void setClassPackage(String classPackage) {
        this.classPackage = classPackage;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getDialect() {
        return dialect;
    }

    public void setDialect(String dialect) {
        this.dialect = dialect;
    }
}
