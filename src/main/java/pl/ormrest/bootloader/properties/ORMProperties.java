package pl.ormrest.bootloader.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Properties;

public final class ORMProperties {

    /**
     * @constructor
     */
    public ORMProperties() {
        oRMPropertiesObject = new ORMPropertiesObject();
        this.initPropertiesFromFile();
    }

    public ORMProperties(ORMPropertiesObject oRMPropertiesObject) {
        ORMProperties.oRMPropertiesObject = oRMPropertiesObject;
        this.initPropertiesFromJSON();
    }
    /**
     * @field
     */
    private static ORMPropertiesObject oRMPropertiesObject;
    private final String mainPath = "/src/pl/ormrest/bootloader/generate/";
    /**
     * @getter/setter
     */
    public static ORMPropertiesObject getoRMPropertiesObject() {
        return oRMPropertiesObject;
    }

    /**
     * @method
     */
    public int createProperties() {
        OutputStream output = null;
        try {
            Properties properties = new Properties();
            output = new FileOutputStream("ormconfig.properties");
            properties.setProperty("orm.driverClassName", "?");
            properties.setProperty("orm.url", "?");
            properties.setProperty("orm.username", "?");
            properties.setProperty("orm.password", "?");
            properties.setProperty("orm.projectPath", "?");
            properties.setProperty("orm.schema", "?");
            properties.setProperty("orm.classPackage", "?");
            properties.setProperty("orm.dialect", "?");
            properties.store(output, null);
            return 0;
        } catch (IOException eSaveError) {
            System.out.println(">>ORMAPP<< Error save ormconfig.properties.");
            System.out.println(eSaveError);
            return 0;
        } finally {
            if (output != null) {
                try {
                    output.close();
                    System.out.println(">>ORMAPP<< create ormconfig.properties.");
                    return 1;
                } catch (IOException e) {
                    System.out.println(">>ORMAPP<< Error save ormconfig.properties.");
                    return 0;
                }
            }
        }
    }
    
    public int initPropertiesFromJSON() {
        if (ORMProperties.oRMPropertiesObject.getProjectPath().equals("")) {
            File file = new File("");
            String path = file.getAbsolutePath();
            oRMPropertiesObject.setProjectPath(path + this.mainPath);
        }
        return 1;
    }

    public int initPropertiesFromFile() {
        InputStream input = null;
        try {
            Properties properties = new Properties();
            input = new FileInputStream("ormconfig.properties");
            properties.load(input);
            oRMPropertiesObject.setDriverClassName(properties.getProperty("orm.driverClassName"));
            oRMPropertiesObject.setUrl(properties.getProperty("orm.url"));
            oRMPropertiesObject.setUsername(properties.getProperty("orm.username"));
            oRMPropertiesObject.setPassword(properties.getProperty("orm.password"));
            /*if (!properties.getProperty("orm.projectPath").equals("")) {
                oRMPropertiesObject.setProjectPath(properties.getProperty("orm.projectPath"));
            } else {*/
                File file = new File("");
                String path = file.getAbsolutePath();
                oRMPropertiesObject.setProjectPath(path + this.mainPath);
            //}
            oRMPropertiesObject.setSchema(properties.getProperty("orm.schema"));
            oRMPropertiesObject.setClassPackage(properties.getProperty("orm.classPackage"));
            oRMPropertiesObject.setDialect(properties.getProperty("orm.dialect"));
            return 0;
        } catch (IOException eErrorLoad) {
            System.out.println(">>ORMAPP<< Error load ormconfig.properties.");
            System.out.println(eErrorLoad);
            return createProperties();
        } finally {
            if (input != null) {
                try {
                    input.close();
                    System.out.println(">>ORMAPP<< Success load ormconfig.properties.");
                    return 1;
                } catch (IOException eErrorLoad) {
                    System.out.println(">>ORMAPP<< Error load ormconfig.properties.");
                    System.out.println(eErrorLoad);
                    return 0;
                }
            }
        }
    }

}
